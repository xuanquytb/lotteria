<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <title id="title">Category</title>
    <link rel="icon" href="img/logo.jpg" type="image/x-icon">
    <style>
        .jumbotron {
            padding: 2rem 1rem;
        }

        #cont {
            min-height: 570px;
        }

        .item-food {
            transition: 0.1s;

        }
    </style>
</head>

<body>
    <?php include 'partials/_dbconnect.php'; ?>
    <?php require 'partials/_nav.php' ?>


    <?php
    $id = $_GET['catid'];
    $sql = "SELECT * FROM `categories` WHERE categorieId = $id";
    $result = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        $catname = $row['categorieName'];
        $catdesc = $row['categorieDesc'];
    }
    ?>

    <!-- food container starts here -->
    <div class="container my-3" id="cont">
        <div class="col-lg-4 text-center bg-light my-3" style="margin:auto;border-top: 2px groove black;border-bottom: 2px groove black;">
            <h2 class="text-center"><span id="catTitle">Items</span></h2>
        </div>
        <div class="row">
            <?php
            $id = $_GET['catid'];
            $sql = "SELECT *,MAX(foodprices.price) AS prices,MAX(foodprices.updateAt) AS latest_date FROM food JOIN foodprices ON food.foodId = foodprices.idFood JOIN categories on food.foodCategorieId = categories.categorieId WHERE foodCategorieId = $id GROUP BY idFood
            HAVING latest_date IS NOT NULL";
            $result = mysqli_query($conn, $sql);
            $noResult = true;
            while ($row = mysqli_fetch_assoc($result)) {
                $noResult = false;
                $foodId = $row['foodId'];
                $foodName = $row['foodName'];
                $foodPrice = $row['prices'];
                $foodDesc = $row['foodDesc'];
                $foodDesc = $row['foodDesc'];
                $discount = $row['discount'];

                echo '<div class="col-xs-3 col-sm-3 col-md-3">
                        <div class="card" style="width: 17rem;">
                            <img src="img/food-' . $foodId . '.jpg" class="card-img-top" alt="image for this food" width="249px" height="270px">
                            <div class="card-body">
                                <h5 class="card-title">' . substr($foodName, 0, 20) . '...</h5>
                                <p class="card-text" style="font-size:18px; color: #6b6c6c ;font-weight:700;margin-bottom: 5px;text-decoration: line-through;">' . $foodPrice . ' ₫</p>
                                <p class="card-text" style="font-size:16px; color: #ff5b6a;font-weight:500;  " > <img class="mr-2" src="http://localhost/lotte/assets/svg/tag.svg" alt="">' . $foodPrice - ($foodPrice * $discount / 100) . ' ₫</p>
                                <p class="card-text">' . substr($foodDesc, 0, 29) . '...</p>   
                                <div class="row justify-content-center">';
                if ($loggedin) {
                    $quaSql = "SELECT `itemQuantity` FROM `viewcart` WHERE foodId = '$foodId' AND `userId`='$userId'";
                    $quaresult = mysqli_query($conn, $quaSql);
                    $quaExistRows = mysqli_num_rows($quaresult);
                    if ($quaExistRows == 0) {
                        echo '<form action="partials/_manageCart.php" method="POST">
                                              <input type="hidden" name="itemId" value="' . $foodId . '">
                                              <button type="submit" name="addToCart" class="btn btn-primary mx-2">Thêm vào giỏ hàng</button>';
                    } else {
                        echo '<form action="partials/_manageCart.php" method="POST">
                        <input type="hidden" name="itemId" value="' . $foodId . '">
                        <button type="submit" name="addToCart" class="btn btn-primary mx-2">Thêm vào giỏ hàng</button>';
                    }
                } else {
                    echo '<button class="btn btn-primary mx-2" data-toggle="modal" data-target="#loginModal">Thêm vào giỏ</button>';
                }
                echo '</form>                            
                                <a href="viewfood.php?foodid=' . $foodId . '" class="mx-2"><button class="btn btn-primary">Xem</button></a> 
                                </div>
                            </div>
                        </div>
                    </div>';
            }
            if ($noResult) {
                echo '<div style="
                width: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
            ">
                    
                        <h2 class="">Menu này đang được cập nhật sản phẩm vào thời gian tới.</h2>
                   
                </div> ';
            }
            ?>
        </div>
    </div>


    <?php require 'partials/_footer.php' ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/bootstrap-show-password@1.2.1/dist/bootstrap-show-password.min.js"></script>
    <script>
        document.getElementById("title").innerHTML = "<?php echo $catname; ?>";
        document.getElementById("catTitle").innerHTML = "<?php echo $catname; ?>";
    </script>
</body>

</html>