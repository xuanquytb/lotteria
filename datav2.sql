CREATE DATABASE  IF NOT EXISTS `opd` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `opd`;
-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: opd
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `categorieId` int NOT NULL AUTO_INCREMENT,
  `categorieName` varchar(255) NOT NULL,
  `categorieDesc` text NOT NULL,
  `categorieCreateDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`categorieId`),
  FULLTEXT KEY `categorieName` (`categorieName`,`categorieDesc`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Thức ăn nhẹ','Thức ăn nhẹ','2023-11-13 16:39:48'),(2,'Cơm - Mì Ý','Cơm - Mì Ý','2023-11-13 16:40:26'),(3,'Phần Ăn Nhóm','Phần Ăn Nhóm','2023-11-13 16:41:04'),(4,'Gà rán phần','Gà rán phần','2023-11-13 16:46:36'),(5,'Thức uống','Thức uống','2023-11-13 16:47:20'),(6,'Burger','Burger','2023-11-13 17:21:42'),(7,'Gà rán','Gà rán','2023-11-14 02:55:25'),(8,'đasa','ádsad','2023-11-15 03:25:33'),(9,'Combo','Combo','2023-11-15 05:01:57');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorynews`
--

DROP TABLE IF EXISTS `categorynews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorynews` (
  `CategoryID` int NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorynews`
--

LOCK TABLES `categorynews` WRITE;
/*!40000 ALTER TABLE `categorynews` DISABLE KEYS */;
INSERT INTO `categorynews` VALUES (1,'Món Chínhsdfdsf'),(2,'Đồ Uống'),(3,'Khuyến Mãi'),(4,'Tin Tức Nhà Hàng'),(5,'Sự Kiện Đặc Biệt');
/*!40000 ALTER TABLE `categorynews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact` (
  `contactId` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL,
  `email` varchar(35) NOT NULL,
  `phoneNo` bigint NOT NULL,
  `orderId` int NOT NULL DEFAULT '0' COMMENT 'If problem is not related to the order then order id = 0',
  `message` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contactId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactreply`
--

DROP TABLE IF EXISTS `contactreply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contactreply` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contactId` int NOT NULL,
  `userId` int NOT NULL,
  `message` text NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactreply`
--

LOCK TABLES `contactreply` WRITE;
/*!40000 ALTER TABLE `contactreply` DISABLE KEYS */;
/*!40000 ALTER TABLE `contactreply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverydetails`
--

DROP TABLE IF EXISTS `deliverydetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `deliverydetails` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orderId` int NOT NULL,
  `deliveryBoyName` varchar(35) NOT NULL,
  `deliveryBoyPhoneNo` bigint NOT NULL,
  `deliveryTime` int NOT NULL COMMENT 'Time in minutes',
  `dateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orderId` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverydetails`
--

LOCK TABLES `deliverydetails` WRITE;
/*!40000 ALTER TABLE `deliverydetails` DISABLE KEYS */;
INSERT INTO `deliverydetails` VALUES (1,1,'te',985555555,7,'2023-11-14 05:53:19');
/*!40000 ALTER TABLE `deliverydetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `food` (
  `foodId` int NOT NULL AUTO_INCREMENT,
  `foodName` varchar(255) NOT NULL,
  `foodPrice` int NOT NULL,
  `discount` int DEFAULT NULL,
  `foodDesc` text NOT NULL,
  `foodCategorieId` int NOT NULL,
  `foodPubDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`foodId`),
  FULLTEXT KEY `pizzaName` (`foodName`,`foodDesc`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food`
--

LOCK TABLES `food` WRITE;
/*!40000 ALTER TABLE `food` DISABLE KEYS */;
INSERT INTO `food` VALUES (1,'Micho Sparkling',25000,5,'Micho Sparkling',5,'2023-11-13 16:48:07'),(2,'Pepsi (M)',14000,5,'Pepsi (M)',5,'2023-11-13 16:53:10'),(3,'Gà Pie',44000,23,'Gà Pie',1,'2023-11-13 17:34:19'),(4,'Gà Rán Phần - 1 Phần',88000,23,'Gà Rán Phần - 1 Phần',3,'2023-11-14 01:58:25'),(5,'Gà Rán 6 Miếng - 1 Phần',200000,23,'Gà Rán 6 Miếng - 1 Phần',3,'2023-11-14 01:59:20'),(6,'Gà Sốt Phần - 1 Phần',97000,23,'Gà Sốt Phần - 1 Phần',3,'2023-11-14 02:00:28'),(7,'Gà Nướng (1 Miếng) - 1 Phần',45000,23,'Gà Nướng (1 Miếng) - 1 Phần',1,'2023-11-14 02:01:18'),(8,'Loy Set - 1 Phần',141000,23,'Loy Set - 1 Phần',3,'2023-11-14 02:02:17'),(9,' Burger Double Double - 1 Phần',75000,23,'\r\nBurger Double Double - 1 Phần',6,'2023-11-14 02:03:39'),(10,'Combo Burger Double Double - 1 Phần',104000,23,'Combo Burger Double Double - 1 Phần',6,'2023-11-14 02:04:28'),(11,'Combo Burger LChicken',82000,23,'01 LChicken Burger\r\n\r\n01 Fried Fries \r\n\r\n01 Pepsi (M)',9,'2023-11-15 05:03:07');
/*!40000 ALTER TABLE `food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `NewsID` int NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `Content` text,
  `PublishDate` date DEFAULT NULL,
  `CategoryID` int DEFAULT NULL,
  `ImageURL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`NewsID`),
  KEY `FK_Category_News` (`CategoryID`),
  CONSTRAINT `FK_Category_News` FOREIGN KEY (`CategoryID`) REFERENCES `categorynews` (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'THÁNG 11 - Khuyến mại tưng bừng','<div class=\"promotion-slide\" style=\"margin: 0px 0px 25px; padding: 0px; outline: 0px; color: rgb(20, 47, 67); font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\"><div class=\"swiper swiper-initialized swiper-horizontal swiper-pointer-events overflow-hidden swiper-backface-hidden\" style=\"margin: 0px auto; padding: 0px; outline: 0px; position: relative; list-style: none; z-index: 1; touch-action: pan-y;\"><div class=\"swiper-wrapper\" style=\"box-sizing: content-box; margin: 0px; padding: 0px; outline: 0px; position: relative; width: 1110px; height: 710.516px; z-index: 1; display: flex; transition-property: transform; transform: translate3d(0px, 0px, 0px);\"><div class=\"swiper-slide swiper-slide-active\" style=\"margin: 0px; padding: 0px; outline: 0px; flex-shrink: 0; width: 1110px; height: 710.516px; position: relative; transition-property: transform; transform: translateZ(0px); backface-visibility: hidden;\"><div class=\"item\" style=\"margin: 0px; padding: 0px; outline: 0px; display: flex; justify-content: center; align-items: flex-start; width: 1110px;\"><img src=\"https://cdn.lotteria.vn/media/mageplaza/blog/post/n/s/nsp_thang_11-01.jpg\" loading=\"eager\" style=\"margin: 0px; padding: 0px; outline: 0px; max-width: 100%; height: auto;\"></div></div></div></div></div><h2 class=\"f-title\" style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px; outline: 0px; font-weight: 700; font-size: 24px; color: rgb(255, 91, 106); text-align: center; text-transform: uppercase; font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\">THÁNG 11 - RỘN RÃ CÙNG NHAU</h2><div class=\"f-content-detail\" style=\"margin: 0px; padding: 0px; outline: 0px; color: rgb(20, 47, 67); font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\"><div style=\"margin: 0px; padding: 0px; outline: 0px;\"><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Super Idol&nbsp;<span style=\"margin: 0px; padding: 0px; outline: 0px;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">\"Gà Tứ vị\"&nbsp;</span></em></span>đã trở lại, Fans thoả thích \"mix and match\" đủ loại gà sốt để thưởng thức cùng nhau chỉ với 130.000đ thui nè</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Cớ tới 4 lựa chọn nha: Gà Mê Mật ngọt ngào quyến rũ - Gà H&amp;S cay ngọt siêu cuốn - Gà Sốt Phô Mai béo thơm - Gà Sốt Đậu trọn vị mặn - ngọt -cay</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Fans nào thích \"nhâm nhi\" một mình thì đừng bỏ qua Combo 81.000 này nha: Ga rán + Mỳ Ý + Pepsi + Kem tornado (1 trong 3 vị: Matcha/Strawberry/Chocolate) - tiết kiệm đến 20.000đ</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Combo Couple \"cùng nhau\" 2 Gà rán + Burger tôm + Khoai tây chiên (M) + 2 Pepsi (M) giá chỉ 142.000đ - tiết kiệm 32.000đ</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">\"Vơ-đét\" cho cuộc dzui nè: Combo 3 Gà rán + Burger Bulgogi + Khoai tây lắc + 3 Pepsi (M) =&gt; Giá chỉ 183.000đ - tiết kiệm đến 51.000đ.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Chill Gà ngon hơn - vui hơn với ưu đãi thanh toán qua ví điện tử:</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Giảm ngay 15.000đ cho hoá đơn từ 142.000đ khi thanh toán bằng Zalopay</span></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Giảm 10.000đ cho hoá đơn từ 79.000đ khi thanh toán bằng VNPay - áp dụng vào ngày thứ 6 hằng tuần</span></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Đến liền Lotteria gần nhất \"dựt\" liền ưu đãi tháng 11 này nha Fans ơi!</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\">#LotteriaViatnam #MyFAVORITELOTTERIA #together #tháng11 #cungnhau</em></span></p></div></div>','2023-11-15',1,'news-1.jpg'),(2,'HONEYHOLIC CHICKEN - GÀ MÊ MẬT','<div class=\"promotion-slide\" style=\"margin: 0px 0px 25px; padding: 0px; outline: 0px; color: rgb(20, 47, 67); font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\"><div class=\"swiper swiper-initialized swiper-horizontal swiper-pointer-events overflow-hidden swiper-backface-hidden\" style=\"margin: 0px auto; padding: 0px; outline: 0px; position: relative; list-style: none; z-index: 1; touch-action: pan-y;\"><div class=\"swiper-wrapper\" style=\"box-sizing: content-box; margin: 0px; padding: 0px; outline: 0px; position: relative; width: 1110px; height: 624.891px; z-index: 1; display: flex; transition-property: transform; transform: translate3d(0px, 0px, 0px);\"><div class=\"swiper-slide swiper-slide-active\" style=\"margin: 0px; padding: 0px; outline: 0px; flex-shrink: 0; width: 1110px; height: 624.891px; position: relative; transition-property: transform; transform: translateZ(0px); backface-visibility: hidden;\"><div class=\"item\" style=\"margin: 0px; padding: 0px; outline: 0px; display: flex; justify-content: center; align-items: flex-start; width: 1110px;\"><img src=\"https://cdn.lotteria.vn/media/mageplaza/blog/post/c/o/cover_ga_me_mat-01.jpg\" loading=\"eager\" style=\"margin: 0px; padding: 0px; outline: 0px; max-width: 100%; height: auto;\"></div></div></div></div></div><h2 class=\"f-title\" style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px; outline: 0px; font-weight: 700; font-size: 24px; color: rgb(255, 91, 106); text-align: center; text-transform: uppercase; font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\">HONEYHOLIC CHICKEN - GÀ MÊ MẬT</h2><div class=\"f-content-detail\" style=\"margin: 0px; padding: 0px; outline: 0px; color: rgb(20, 47, 67); font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\"><div style=\"margin: 0px; padding: 0px; outline: 0px;\"><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-size: small; color: rgb(249, 87, 123);\">Ngọt ngào từ hương hoa -&nbsp;<span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\"><span style=\"margin: 0px; padding: 0px; outline: 0px;\">Bạn đã thử chưa?</span></span></span></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-size: small; color: rgb(249, 87, 123);\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\"><span style=\"margin: 0px; padding: 0px; outline: 0px;\"></span></span></span></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\">Lần đầu tiên</em></span>, Lotteria mang đến cho bạn hương vị&nbsp;</span><em style=\"margin: 0px; padding: 0px; outline: 0px;\">ngọt ngào đến từ thiên nhiên</em></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; color: rgb(244, 108, 10);\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Hoà quyện trong từng thớ thịt gà là sốt&nbsp;</span></em><em style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">mật ong cao nguyên, khiến cho bao con tim mê Gà đều xao xuyến.</span></em></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; color: rgb(244, 108, 10);\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Dù bạn là ai - Dù mang phong cách nào - Gà Mê Mật cũng chiều được hết</span></em></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Thưởng thức ngay vị ngon tuyệt đỉnh - Gà Mê Mật tại các cửa hàng Lotteria trên toàn quốc, giá \"đáng iu\" chỉ 40.000/miếng</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Đặc biệt&nbsp;</span>- trong 3 ngày đầu tiên ra mắt</p><h3 style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; color: rgb(246, 70, 8);\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Mua 2 Gà Mê Mật - MIỄN PHÍ 1 Gà cùng loại nữa nha - Mua 2 được 3 - Quá đã</span></span></h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><iframe title=\"YouTube video player\" src=\"https://www.youtube.com/embed/tvNOEO-imUU?si=cMZ5hsxKz5rLQYSv\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen=\"\" style=\"margin: 0px auto; padding: 0px; outline: 0px; max-width: 100%; border-width: initial; border-style: none; display: block;\"></iframe></p><pre xml=\"space\" style=\"padding: 0px 0px 0px 360px; outline: 0px; font-size: 14px; color: rgb(33, 37, 41);\">  <em style=\"margin: 0px; padding: 0px; outline: 0px;\">(Áp dụng từ 25/10 đến 27/10/2023)</em></pre><pre xml=\"space\" style=\"padding: 0px; outline: 0px; font-size: 14px; color: rgb(33, 37, 41);\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"></em></pre><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Tặng ngay&nbsp;<span style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">1 Pepsi Zero/7UP soda chanh</span></span><span style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">&nbsp;</span></span>(lon) khi dùng&nbsp;<span style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Combo Ưu dãi: 2 Gà Mê Mật + Pepsi (M) giá chỉ 75.000.</span></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Chưa hết đâu nha: khi dùng Gà Mê Mật Chicken set giá 93.000: ⚡️<span style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">MIỄN PHÍ Upsize - Khoai tây chiên =&gt; Khoai tây lắc &amp; Pepsi (M) =&gt; Pepsi (L)</span></span></p><pre xml=\"space\" style=\"padding: 0px; outline: 0px; font-size: 14px; color: rgb(33, 37, 41);\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\">&nbsp;(Áp dụng từ 25/10 đến 26/11/2023)</em></pre><pre xml=\"space\" style=\"padding: 0px; outline: 0px; font-size: 14px; color: rgb(33, 37, 41);\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"></em></pre><h3 style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; color: rgb(255, 0, 0);\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Nhắn cho Hội mê gà đến Lotteria \"check\" liền chiếc Gà siêu \"quyến rũ\" này nha!</span></span></h3><pre xml=\"space\" style=\"padding: 0px; outline: 0px; font-size: 14px; color: rgb(33, 37, 41);\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"></em></pre><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\">#LotteriaVietnam #MyFAVORITELOTTERIA #Honeyholicchicken #Gamemat #new</em></span></p></div></div>','2023-11-15',3,'news-2.jpg'),(3,'CHICKEN TERIYAKI PIZZA','<div class=\"f-img\" style=\"margin: 0px 0px 30px; padding: 0px; outline: 0px; text-align: center; color: rgb(20, 47, 67); font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\"><img src=\"https://cdn.lotteria.vn/media/mageplaza/blog/post/p/i/pizza_teriyaki-01.jpg\" alt=\"CHICKEN TERIYAKI PIZZA\" style=\"margin: 0px; padding: 0px; outline: 0px; max-width: 100%; height: auto;\"></div><h2 class=\"f-title\" style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px; outline: 0px; font-weight: 700; font-size: 24px; color: rgb(255, 91, 106); text-align: center; text-transform: uppercase; font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\">CHICKEN TERIYAKI PIZZA</h2><div class=\"f-content-detail\" style=\"margin: 0px; padding: 0px; outline: 0px; color: rgb(20, 47, 67); font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\"><div style=\"margin: 0px; padding: 0px; outline: 0px;\"><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Không chỉ có Gà rán và Burger, Lotteria mong muốn gửi đến các Fans thêm nhiều lựa chọn cho những bữa ăn khác trong ngày</span></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Cùng chào đón “Snack” mới tại nhà Gà:&nbsp;<span style=\"margin: 0px; padding: 0px; outline: 0px;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Pizza Gà Teriyaki</span></em></span>&nbsp;là kết hợp giữa đế bánh giòn thơm với hương vị mặn-ngọt đặc trưng của&nbsp;<span style=\"margin: 0px; padding: 0px; outline: 0px;\">sốt Teriyaki</span>, hoà quyện với&nbsp;<span style=\"margin: 0px; padding: 0px; outline: 0px;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\">phô mai Mozzarella tan chảy,</em></span>&nbsp;thêm chút giòn thơm từ&nbsp;<span style=\"margin: 0px; padding: 0px; outline: 0px;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\">nấm và thịt gà tươi&nbsp;</em></span>mềm mọng nước.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px; text-align: center;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-size: small; color: rgb(84, 137, 36);\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\">Chiếc Pizza \"mới toe\" này lên kệ từ ngày 01/08/2023</em></span></span></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px; text-align: center;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-size: small; color: rgb(84, 137, 36);\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Giá chỉ 38.000đ&nbsp;</span></em>– Thử ngay tại các cửa hàng Lotteria trên toàn quốc.</span></p></div></div>','2023-11-15',5,'news-3.jpg'),(4,'LOTTERIA ĐÃ CÓ MẶT TẠI GAME PLAY TOGETHER','<div class=\"promotion-slide\" style=\"margin: 0px 0px 25px; padding: 0px; outline: 0px; color: rgb(20, 47, 67); font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\"><div class=\"swiper swiper-initialized swiper-horizontal swiper-pointer-events overflow-hidden swiper-backface-hidden\" style=\"margin: 0px auto; padding: 0px; outline: 0px; position: relative; list-style: none; z-index: 1; touch-action: pan-y;\"><div class=\"swiper-wrapper\" style=\"box-sizing: content-box; margin: 0px; padding: 0px; outline: 0px; position: relative; width: 1110px; height: 750px; z-index: 1; display: flex; transition-property: transform; transform: translate3d(0px, 0px, 0px);\"><div class=\"swiper-slide swiper-slide-active\" style=\"margin: 0px; padding: 0px; outline: 0px; flex-shrink: 0; width: 1110px; height: 750px; position: relative; transition-property: transform; transform: translateZ(0px); backface-visibility: hidden;\"><div class=\"item\" style=\"margin: 0px; padding: 0px; outline: 0px; display: flex; justify-content: center; align-items: flex-start; width: 1110px;\"><img src=\"https://cdn.lotteria.vn/media/mageplaza/blog/post/p/l/playtogether_web.jpg\" loading=\"eager\" style=\"margin: 0px; padding: 0px; outline: 0px; max-width: 100%; height: auto;\"></div></div></div></div></div><h2 class=\"f-title\" style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px; outline: 0px; font-weight: 700; font-size: 24px; color: rgb(255, 91, 106); text-align: center; text-transform: uppercase; font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\">LOTTERIA ĐÃ CÓ MẶT TẠI GAME PLAY TOGETHER</h2><div class=\"f-content-detail\" style=\"margin: 0px; padding: 0px; outline: 0px; color: rgb(20, 47, 67); font-family: Roboto, Arial, sans-serif; background-color: rgb(255, 252, 254);\"><div style=\"margin: 0px; padding: 0px; outline: 0px;\"><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Hế lô các thần dân của đảo Kaia, Lotteria vừa \"khai trương\" một cửa hàng mới tại khu trung tâm đó.</span></em></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\"><em style=\"margin: 0px; padding: 0px; outline: 0px;\"><span style=\"margin: 0px; padding: 0px; outline: 0px; font-weight: bolder;\">Các bạn đã đến trải nghiệm chưa?</span></em></p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Cùng tham gia làm nhiệm vụ tìm gà rán để trang bị thêm mũ Lody cho tủ đồ của mình nha.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Bạn nào muốn \"độ da\" sang màu đỏ nâu khỏe khoắn thì thử liền món Gà rán H&amp;S best seller tại cửa hàng Lotteria trên Play Together nhé!</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Cùng chờ đón những sự kiện thú vị và vật phẩm hấp dẫn tại cửa hàng ảo Lotteria nha!</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; outline: 0px;\">Hẹn gặp lại các bạn ở khu trung tâm đảo Kaia!!!</p></div></div>','2023-11-15',5,'news-4.jpg');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderitems`
--

DROP TABLE IF EXISTS `orderitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orderitems` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orderId` int NOT NULL,
  `foodId` int NOT NULL,
  `itemQuantity` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderitems`
--

LOCK TABLES `orderitems` WRITE;
/*!40000 ALTER TABLE `orderitems` DISABLE KEYS */;
INSERT INTO `orderitems` VALUES (1,1,6,2);
/*!40000 ALTER TABLE `orderitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `orderId` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL,
  `address` varchar(255) NOT NULL,
  `phoneNo` bigint NOT NULL,
  `amount` int NOT NULL,
  `paymentMode` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=cash on delivery, \r\n1=online ',
  `orderStatus` enum('0','1','2','3','4','5','6') NOT NULL DEFAULT '0' COMMENT '0=Order Placed.\r\n1=Order Confirmed.\r\n2=Preparing your Order.\r\n3=Your order is on the way!\r\n4=Order Delivered.\r\n5=Order Denied.\r\n6=Order Cancelled.',
  `orderDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,'TAY SON KIEN XUONG THAI BINH',111111111,194000,'0','4','2023-11-14 05:20:18');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotionevents`
--

DROP TABLE IF EXISTS `promotionevents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promotionevents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `description` text,
  `discount_percentage` decimal(5,2) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotionevents`
--

LOCK TABLES `promotionevents` WRITE;
/*!40000 ALTER TABLE `promotionevents` DISABLE KEYS */;
INSERT INTO `promotionevents` VALUES (67,'XuanQuy2723','2023-11-20',NULL,'rrrr',99.00,1),(68,'rrrr','2023-11-20',NULL,'rrrr',23.00,1),(69,'rtỷty','2023-11-20','2023-11-20','yyyyy',99.00,1);
/*!40000 ALTER TABLE `promotionevents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitedetail`
--

DROP TABLE IF EXISTS `sitedetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sitedetail` (
  `tempId` int NOT NULL AUTO_INCREMENT,
  `systemName` varchar(21) NOT NULL,
  `email` varchar(35) NOT NULL,
  `contact1` bigint NOT NULL,
  `contact2` bigint DEFAULT NULL COMMENT 'Optional',
  `address` text NOT NULL,
  `dateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tempId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitedetail`
--

LOCK TABLES `sitedetail` WRITE;
/*!40000 ALTER TABLE `sitedetail` DISABLE KEYS */;
INSERT INTO `sitedetail` VALUES (1,'Lotteria','lotteria@gmail.com',868603825,969523424,'Lạch tray','2021-03-23 19:56:25');
/*!40000 ALTER TABLE `sitedetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(21) NOT NULL,
  `firstName` varchar(21) NOT NULL,
  `lastName` varchar(21) NOT NULL,
  `email` varchar(35) NOT NULL,
  `phone` bigint NOT NULL,
  `userType` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=user\r\n1=admin',
  `password` varchar(255) NOT NULL,
  `joinDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lock` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','admin','admin@gmail.com',1111111111,'1','$2y$10$AAfxRFOYbl7FdN17rN3fgeiPu/xQrx6MnvRGzqjVHlGqHAM4d9T1i','2021-04-11 11:40:58','0'),(2,'user','VU','QUY','vuxuanquynnn@gmail.com',868603825,'0','$2y$10$7gkJyQDskRw599nAUEnaPOyjdCjlRvMSLrCIORD8ZqeHDvvYM2AiW','2023-11-13 06:54:09','0'),(3,'admin2','VU','QUY','vuxuanquynnn@gmail.com',868603825,'1','$2y$10$TYHAX/GTLCQ541IdEbOUjuvvW3.4drk6ZULY9Lt8.iUcFKT8BufzG','2023-11-14 17:50:12','0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viewcart`
--

DROP TABLE IF EXISTS `viewcart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `viewcart` (
  `cartItemId` int NOT NULL AUTO_INCREMENT,
  `foodId` int NOT NULL,
  `itemQuantity` int NOT NULL,
  `userId` int NOT NULL,
  `addedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cartItemId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viewcart`
--

LOCK TABLES `viewcart` WRITE;
/*!40000 ALTER TABLE `viewcart` DISABLE KEYS */;
INSERT INTO `viewcart` VALUES (11,4,2,2,'2023-11-14 15:48:01'),(12,3,1,1,'2023-11-14 15:58:33'),(13,5,1,2,'2023-11-15 07:14:52');
/*!40000 ALTER TABLE `viewcart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'opd'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `discount_event_20231120` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `discount_event_20231120` ON SCHEDULE AT '2023-11-20 10:34:00' ON COMPLETION PRESERVE DISABLE DO BEGIN
                        UPDATE `opd`.`food` SET `discount` = 99 where foodCategorieId = 5;
                    END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `end_discount_event_20231120` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `end_discount_event_20231120` ON SCHEDULE AT '2023-11-20 10:35:00' ON COMPLETION PRESERVE DISABLE DO BEGIN
                        UPDATE `opd`.`food` SET `discount` = 5 where foodCategorieId = 5;
                    END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `one_discount_event_20231120` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `one_discount_event_20231120` ON SCHEDULE AT '2023-11-20 10:32:00' ON COMPLETION PRESERVE DISABLE DO BEGIN
                        UPDATE `opd`.`food` SET `discount` = 23;
                    END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `one_end_discount_event_20231120` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb4 */ ;;
/*!50003 SET character_set_results = utf8mb4 */ ;;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `one_end_discount_event_20231120` ON SCHEDULE AT '2023-11-20 23:59:00' ON COMPLETION PRESERVE ENABLE DO BEGIN
                        UPDATE `opd`.`food` SET `discount` = 5;
                    END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'opd'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-11-20 10:44:13
