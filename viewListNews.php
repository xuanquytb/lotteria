<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <title id="title">Category</title>
    <link rel="icon" href="img/logo.jpg" type="image/x-icon">
    <style>
    .jumbotron {
        padding: 2rem 1rem;
    }

    #cont {
        min-height: 570px;
    }

    .item-food {
        transition: 0.1s;

    }
    </style>
</head>

<body>
    <?php include 'partials/_dbconnect.php'; ?>
    <?php require 'partials/_nav.php' ?>





    <!-- food container starts here -->
    <div class="container my-3" id="cont">
        <div class="col-lg-4 text-center bg-light my-3"
            style="margin:auto;border-top: 2px groove black;border-bottom: 2px groove black;">
            <h2 class="text-center"><span id="catTitle">Tin tức</span></h2>
        </div>
        <div class="row" style="gap: 10px;">
            <!-- Tin tức -->

            <!-- Fetch all the categories and use a loo p to iterate through categories -->
            <?php
            $id = $_GET['catid'];
            $sql = "SELECT * FROM opd.News where CategoryID = $id";
            $result = mysqli_query($conn, $sql);
            while ($row = mysqli_fetch_assoc($result)) {
                $id = $row['NewsID'];
                $title = $row['Title'];
                $content = $row['Content'];
                $PublishDate = $row['PublishDate'];
                $discount = $row['ImageURL'];
                echo '
                            <a href="http://localhost/lotte/viewnews.php?newsid=' . $id . '" >
                            <div class=" mb-4" >
                                <div class="card-item" style="width: 17rem;max-height:388px;box-shadow : #dfd5d5 1px 1px 5px 0px;">
                                <img src="img/news-' . $id . '.jpg" class="card-img-top" alt="image for this category" width="249px" height="150px">
                                <div class="card-body" style="padding:0.5rem;">
                                    <h5 class="card-title" style="min-height:48px"><a style="font-size: 16px;color: #142f43;" href="http://localhost/lotte/viewnews.php?newsid=' . $id . '">' . $title . '</a></h5>
                                    <p class="card-text" style="font-size:12px;color: #gray;font-weight:700;">' . date("d-m-Y", strtotime($PublishDate)) . '</p>
                                </div>
                                <div style="display: grid;place-items: center; margin-bottom:10px">
                                    <a href="http://localhost/lotte/viewnews.php?newsid=' . $id . '" >Xem thêm</a>
                                </div>
                                </div>
                            </div>
                            </a>
                            ';
            }
            ?>



        </div>
    </div>


    <?php require 'partials/_footer.php' ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/bootstrap-show-password@1.2.1/dist/bootstrap-show-password.min.js"></script>
    <script>
    document.getElementById("title").innerHTML = "<?php echo $catname; ?>";
    document.getElementById("catTitle").innerHTML = "<?php echo $catname; ?>";
    </script>
</body>

</html>