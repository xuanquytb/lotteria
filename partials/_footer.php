<div class="footer">
    <div class="footer-in">
        <div class="regis">
            <img class="rounded-circle" src="http://localhost/lotte/assets/img/logo/lotteria.png" width="80px"
                height="80px" alt="">
            <h6>Cảm ơn quý khách</h6>

        </div>

        <div class="box1">
            <h6>THÔNG TIN</h6>
            <span><a href="viewListNewsAll.php">Tin tức</a></span>
            <span><a href="viewListNews.php?catid='3'">Khuyến mại</a></span>
            <span>Tuyển dụng</span>
            <span>Nhượng quyền</span>
        </div>
        <div class="box2">
            <h6>HỖ TRỢ</h6>
            <span>Điều khoản sử dụng</span>
            <span>Chính sách bảo mật</span>
            <span>Chính sách giao hàng</span>
            <span>Chăm Sóc Khách Hàng</span>
        </div>
        <div class="box3">
            <h6>THEO DÕI</h6>
            <span>fakebook</span>
            <span>Instagram</span>
            <span>Zalo</span>
            <span>X</span>

        </div>
    </div>

    <div>

    </div>
</div>


<style>
.footer-in {
    display: grid;
    width: 100%;
    height: 250px;
    grid-template-areas: "regis box1 box2 box2";
    grid-template-rows: 50px 1fr 30px;
    grid-template-columns: 400px 1fr 1fr 1fr;
    padding: 0 150px;
    place-items: center;
    transform: translateY(129px);
}

.regis {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.regis img {
    margin-bottom: 10px;
}

.regis h6 {
    margin-bottom: 20px;
}

.box1 {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: start;
}

.box2 {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: start;
}

.box3 {
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: start;
    transform: translateX(-30px);
}

.input-mail {
    display: flex;
    justify-content: space-around;
    background-color: #ffffff;
    border-radius: 4px;
    box-shadow: #c4b9b9 0px 1px 4px 0px;
    color: #142f43;
    line-height: 22.4px;
    padding: 8px 6px;
}

.input-mail input {
    border: unset;
    width: 250px;
}

.input-mail button {
    align-items: flex-start;
    background-position: 0px 0px;
    color: #ff5b6a;
    display: inline-block;
    font-weight: 700;
    line-height: 24px;
    padding: 5px;
    text-align: center;
    border: unset;
    background-color: transparent;
    margin-left: 20px;
}

.footer {
    min-width: 100vw;
    min-height: 338px;
    background-color: #ffeae3;
    color: #142f43;
    line-height: 22.4px;
    text-align: left;
}
</style>