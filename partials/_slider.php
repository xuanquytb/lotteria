<?php
// Kết nối đến cơ sở dữ liệu
// Thực hiện truy vấn để lấy danh sách các danh mục từ cơ sở dữ liệu
// Ví dụ:
$categories = array("Category 1", "Category 2", "Category 3", "Category 4", "Category 5", "Category 6");
?>


<div class="swiper-container" style="max-width:100%; margin: 0 auto; overflow: hidden; margin-top: 30px;">
  <div class="swiper-wrapper">
    <?php
    $sql = "SELECT * FROM `categories`";
    $result = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
      $id = $row['categorieId'];
      $cat = $row['categorieName'];
      $desc = $row['categorieDesc'];
      echo '
        <a  href="viewfoodList.php?catid=' . $id . '" >
          <div class=" swiper-slide" style="display: grid !important;place-items: center;">
            <div style="max-height:372px;">
              <img src="img/card-' . $id . '.jpg" alt="image for this category" width="98px" height="98px" style="border-radius: 15px;">
              <div style="padding:5px; display: grid !important;place-items: center;">
                <p class="card-text"><a href="viewfoodList.php?catid=' . $id . '">' . $cat . '</a></p>
              </div>
            </div>
          </div>
        </a>
        ';
    }
    ?>
  </div>
</div>
<script>
  var swiper = new Swiper('.swiper-container', {
    slidesPerView: 6,
    spaceBetween: 20,
    loop: true,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    // Các tùy chọn khác có thể được thêm vào theo ý muốn
  });
</script>