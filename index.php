<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <!-- CSS Swiper -->
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

  <!-- JS Swiper -->
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

  <title>Home</title>
  <link rel="icon" href="img/logo.jpg" type="image/x-icon">
</head>

<body>
  <?php include 'partials/_dbconnect.php'; ?>
  <?php require 'partials/_nav.php' ?>



  <!-- Category container starts here -->
  <div class="container my-3 mb-5">

    <?php require 'partials/_search.php' ?>
    <?php require 'partials/_slider.php' ?>
    <div class="col-lg-3 mt-5 mb-4 ">
      <h3 class="text-left">Top bán chạy</h3>
    </div>
    <div class="row">
      <!-- Fetch all the categories and use a loop to iterate through categories -->
      <?php
      $sql = "SELECT * FROM `categories` limit 0,4;";
      $result = mysqli_query($conn, $sql);
      while ($row = mysqli_fetch_assoc($result)) {
        $id = $row['categorieId'];
        $cat = $row['categorieName'];
        $desc = $row['categorieDesc'];
        echo '
        <a href="viewfoodList.php?catid=' . $id . '" >
          <div class="col-xs-3 col-sm-3 col-md-3 ">
            <div class="card" style="width: 17rem;max-height:372px;">
              <img src="img/card-' . $id . '.jpg" class="card-img-top" alt="image for this category" width="249px" height="270px">
              <div class="card-body">
                <h5 class="card-title"><a href="viewfoodList.php?catid=' . $id . '">' . $cat . '</a></h5>
                <p class="card-text">' . substr($desc, 0, 30) . '... </p>
                
              </div>
            </div>
          </div>
        </a>
        ';
      }
      ?>

    </div>


    <div class="col-lg-3 mt-5 mb-4">
      <h3 class="text-left">Phần ăn nhóm</h2>
    </div>
    <div class="row">
      <!-- Fetch all the categories and use a loop to iterate through categories -->
      <?php
      $sql = "SELECT *,MAX(foodprices.price) AS prices,MAX(foodprices.updateAt) AS latest_date FROM food JOIN foodprices ON food.foodId = foodprices.idFood JOIN categories on food.foodCategorieId = categories.categorieId where foodCategorieId = 3 GROUP BY idFood HAVING latest_date IS NOT NULL limit 0,4;";
      $result = mysqli_query($conn, $sql);
      while ($row = mysqli_fetch_assoc($result)) {
        $id = $row['foodId'];
        $cat = $row['foodName'];
        $price = $row['prices'];
        $discount = $row['discount'];
        echo '
        <a href="http://localhost/lotte/viewfood.php?foodid=' . $id . '" >
          <div class="col-xs-3 col-sm-3 col-md-3 mb-4" >
            <div class="card-item" style="width: 17rem;max-height:388px;box-shadow : #dfd5d5 1px 1px 5px 0px;">
              <img src="img/food-' . $id . '.jpg" class="card-img-top" alt="image for this category" width="249px" height="270px">
              <div class="card-body" style="padding:1rem;">
                <h5 class="card-title" ><a style="font-size: 16px;color: #142f43;" href="http://localhost/lotte/viewfood.php?foodid=' . $id . '">' . $cat . '</a></h5>
                <p class="card-text" style="font-size:18px; color: #6b6c6c ;font-weight:700;margin-bottom: 5px;text-decoration: line-through;">' . $price . ' ₫</p>
                <p class="card-text" style="font-size:16px; color: #ff5b6a;font-weight:500;  " > <img class="mr-2" src="http://localhost/lotte/assets/svg/tag.svg" alt="">' . $price - ($price * $discount / 100) . ' ₫</p>
              </div>
            </div>
          </div>
        </a>
        ';
      }
      ?>
    </div>
    <!-- Combo -->
    <div class="col-lg-3 mt-5 mb-4">
      <h3 class="text-left">Combo</h2>
    </div>
    <div class="row">
      <!-- Fetch all the categories and use a loop to iterate through categories -->
      <?php
      $sql = "SELECT *,MAX(foodprices.price) AS prices,MAX(foodprices.updateAt) AS latest_date FROM food JOIN foodprices ON food.foodId = foodprices.idFood JOIN categories on food.foodCategorieId = categories.categorieId where foodCategorieId = 9 GROUP BY idFood HAVING latest_date IS NOT NULL limit 0,4;";
      $result = mysqli_query($conn, $sql);
      while ($row = mysqli_fetch_assoc($result)) {
        $id = $row['foodId'];
        $cat = $row['foodName'];
        $price = $row['prices'];
        $discount = $row['discount'];
        echo '
        <a href="http://localhost/lotte/viewfood.php?foodid=' . $id . '" >
          <div class="col-xs-3 col-sm-3 col-md-3 mb-4" >
            <div class="card-item" style="width: 17rem;max-height:388px;box-shadow : #dfd5d5 1px 1px 5px 0px;">
              <img src="img/food-' . $id . '.jpg" class="card-img-top" alt="image for this category" width="249px" height="270px">
              <div class="card-body" style="padding:1rem;">
                <h5 class="card-title" ><a style="font-size: 16px;color: #142f43;" href="http://localhost/lotte/viewfood.php?foodid=' . $id . '">' . $cat . '</a></h5>
                <p class="card-text" style="font-size:18px; color: #6b6c6c ;font-weight:700;margin-bottom: 5px;text-decoration: line-through;">' . $price . ' ₫</p>
                <p class="card-text" style="font-size:16px; color: #ff5b6a;font-weight:500;  " > <img class="mr-2" src="http://localhost/lotte/assets/svg/tag.svg" alt="">' . $price - ($price * $discount / 100) . ' ₫</p>
              </div>
            </div>
          </div>
        </a>
        ';
      }
      ?>
    </div>
    <!-- Tin tức -->
    <div class="col-lg-12 mt-5 mb-4" style="display: flex; justify-content: space-between; align-items: center;">
      <h3 class="text-left">Tin tức</h2>
        <div>
          <a href="viewListNews.php?catid=">Tất cả</a>
        </div>
    </div>
    <div class="row">
      <!-- Fetch all the categories and use a loop to iterate through categories -->
      <?php
      $sql = "SELECT * FROM opd.News LIMIT 0,4";
      $result = mysqli_query($conn, $sql);
      while ($row = mysqli_fetch_assoc($result)) {
        $id = $row['NewsID'];
        $title = $row['Title'];
        $content = $row['Content'];
        $PublishDate = $row['PublishDate'];
        $discount = $row['ImageURL'];
        echo '
        <a href="http://localhost/lotte/viewnews.php?newsid=' . $id . '" >
          <div class="col-xs-3 col-sm-3 col-md-3 mb-4" >
            <div class="card-item" style="width: 17rem;max-height:388px;box-shadow : #dfd5d5 1px 1px 5px 0px;">
              <img src="img/news-' . $id . '.jpg" class="card-img-top" alt="image for this category" width="249px" height="150px">
              <div class="card-body" style="padding:0.5rem;">
                <h5 class="card-title" style="min-height:48px"><a style="font-size: 16px;color: #142f43;" href="http://localhost/lotte/viewnews.php?newsid=' . $id . '">' . $title . '</a></h5>
                <p class="card-text" style="font-size:12px;color: #gray;font-weight:700;">' . date("d-m-Y", strtotime($PublishDate)) . '</p>
              </div>
              <div style="display: grid;place-items: center; margin-bottom:10px">
                <a href="http://localhost/lotte/viewnews.php?newsid=' . $id . '" >Xem thêm</a>
              </div>
            </div>
          </div>
        </a>
        ';
      }
      ?>
    </div>
  </div>


  <?php require 'partials/_footer.php' ?>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
  </script>
  <script src="https://unpkg.com/bootstrap-show-password@1.2.1/dist/bootstrap-show-password.min.js"></script>
</body>

<style>
  .cart-item {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    /* border: 1px solid rgba(0,0,0,.125); */
    border-radius: 0.25rem;
  }
</style>

</html>