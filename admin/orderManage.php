<div class="container" style="margin-top:98px;background: aliceblue;">
    <div class="table-wrapper p-3">
        <div class="table-title" style="border-radius: 14px;">
            <div class="row">
                <div class="col-sm-4">
                    <h2>Quản lý đơn hàng</h2>
                </div>
                <div class="col-sm-8">
                    <a href="" class="btn btn-primary"><i class="material-icons">&#xE863;</i> </a>
                    <a href="#" onclick="window.print()" class="btn btn-info"><i class="material-icons">&#xE24D;</i>
                    </a>
                </div>
            </div>
        </div>
        <form action="" method="post">
            <div>
                <div class="input-group mb-3">
                    <div style="display: flex;gap: 20px;">
                        <select name="status" id="status" class="custom-select browser-default">
                            <option hidden disabled selected value>Chọn danh mục</option>
                            <option value="1">Chờ xác nhận</option>
                            <option value="2">Đã xác nhận</option>
                            <option value="3">Đang chuẩn bị</option>
                            <option value="4">Đang giao hàng</option>
                            <option value="5">Đã giao hàng</option>
                            <option value="6">Không hợp lệ</option>
                            <option value="7">Đã bị hủy</option>
                        </select>

                    </div>
                    <button class="btn btn-outline-secondary mx-2" type="submit">Lọc</button>
                    <button class="btn btn-outline-secondary" type="submit" name="reset" onclick="resetFilters()">Reset</button>
                </div>
            </div>
            <table class="table table-striped table-hover text-center" id="NoOrder">
                <thead style="background-color: rgb(111 202 203);">
                    <tr>
                        <th>Mã đơn</th>
                        <th>Id khách hàng</th>
                        <th>Địa chỉ</th>
                        <th>Số điện thoại</th>
                        <th>Số lượng</th>
                        <th>Phương thức TT</th>
                        <th>Ngày đặt</th>
                        <th>Trạng thái</th>
                        <th>Đơn hàng</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    if (isset($_POST['reset'])) {
                        $status =  '';
                    }

                    $status = $_POST['status'] ?? '';
                    if (isset($_POST['currentPage'])) {
                        $currentPage = $_POST['currentPage'];
                    } else {
                        $currentPage = 1;
                    }
                    $page = $currentPage;

                    $limit = 5;
                    $start = ($page - 1) * $limit;
                    $sql = "SELECT * FROM `orders` ";

                    if (!empty($status)) {
                        $sql .= " WHERE orderStatus = '$status'";
                    }
                    $sql .= " ORDER BY `orderDate` DESC limit $start,$limit";

                    $result = mysqli_query($conn, $sql);
                    $counter = 0;
                    while ($row = mysqli_fetch_assoc($result)) {
                        $Id = $row['userId'];
                        $orderId = $row['orderId'];
                        $address = $row['address'];
                        $phoneNo = $row['phoneNo'];
                        $amount = $row['amount'];
                        $orderDate = $row['orderDate'];
                        $paymentMode = $row['paymentMode'];

                        if ($paymentMode == 0) {
                            $paymentMode = "Cash on Delivery";
                        } else {
                            $paymentMode = "Online";
                        }
                        $orderStatus = $row['orderStatus'];
                        $counter++;

                        echo '<tr>
                                <td>' . $orderId . '</td>
                                <td>' . $Id . '</td>
                                <td data-toggle="tooltip" title="' . $address . '">' . substr($address, 0, 20) . '...</td>
                                <td>' . $phoneNo . '</td>
                                <td>' . $amount . '</td>
                                <td>' . $paymentMode . '</td>
                                <td>' . $orderDate . '</td>
                                <td><a href="#" data-toggle="modal" data-target="#orderStatus' . $orderId . '" class="view"><img src="../assets/svg/deli.svg" alt=""></a></td>
                                <td><a href="#" data-toggle="modal" data-target="#orderItem' . $orderId . '" class="view" title="View Details"><img src="../assets/svg/eye.svg" alt=""></a></td>
                            </tr>';
                    }
                    if ($counter == 0) {
                    ?><script>
                            document.getElementById("NoOrder").innerHTML =
                                '<div class="alert alert-info alert-dismissible fade show" role="alert" style="width:100%"> Chưa có đơn hàng!	</div>';
                        </script> <?php
                                }
                                    ?>
                </tbody>
            </table>
        </form>

        <ul class="pagination" style="display: flex; gap : 5px ; margin-top: 20px; margin-bottom: 20px;">
            <?php
            $productsPerPage = 5;
            $sqlTotal = "SELECT COUNT(*) as total FROM orders";
            $totalResult = $conn->query($sqlTotal);
            $totalRow = $totalResult->fetch_assoc()['total'];
            $totalPages = ceil($totalRow / $productsPerPage);
            if (isset($_POST['currentPage'])) {
                $currentPage = $_POST['currentPage'];
            } else {
                $currentPage = 1;
            }
            $counter = 1;
            while ($counter <= $totalPages) {
                echo "<li class='page-item' >
                        <form method='post' action=''>
                            <input type='hidden' name='currentPage' value='$counter'>
                            <button style=' border: none; width: 60px ;border-radius: 5px;' type='submit'>$counter</button>
                        </form>
                    </li>";
                $counter++;
            } ?>

        </ul>
    </div>
</div>

<script>
    function resetFilters() {
        document.getElementById("status").selectedIndex = ""; // Reset dropdown

        window.location.reload();
    }
</script>

<?php
include 'partials/_orderItemModal.php';
include 'partials/_orderStatusModal.php';
?>

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>
    .tooltip.show {
        top: -62px !important;
    }

    .table-wrapper .btn {
        float: right;
        color: #333;
        background-color: #fff;
        border-radius: 3px;
        border: none;
        outline: none !important;
        margin-left: 10px;
    }

    .table-wrapper .btn:hover {
        color: #333;
        background: #f2f2f2;
    }

    .table-wrapper .btn.btn-primary {
        color: #fff;
        background: #03A9F4;
    }

    .table-wrapper .btn.btn-primary:hover {
        background: #03a3e7;
    }

    .table-title .btn {
        font-size: 13px;
        border: none;
    }

    .table-title .btn i {
        float: left;
        font-size: 21px;
        margin-right: 5px;
    }

    .table-title .btn span {
        float: left;
        margin-top: 2px;
    }

    .table-title {
        color: #fff;
        background: #4b5366;
        padding: 16px 25px;
        margin: -20px -25px 10px;
        border-radius: 3px 3px 0 0;
    }

    .table-title h2 {
        margin: 5px 0 0;
        font-size: 24px;
    }

    table.table tr th,
    table.table tr td {
        border-color: #e9e9e9;
        padding: 12px 15px;
        vertical-align: middle;
    }

    table.table tr th:first-child {
        width: 60px;
    }

    table.table tr th:last-child {
        width: 80px;
    }

    table.table-striped tbody tr:nth-of-type(odd) {
        /* background-color: #fcfcfc; */
    }

    table.table-striped.table-hover tbody tr:hover {
        /* background: #f5f5f5; */
    }

    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }

    table.table td a {
        font-weight: bold;
        color: #566787;
        display: inline-block;
        text-decoration: none;
    }

    table.table td a:hover {
        color: #2196F3;
    }

    table.table td a.view {
        width: 30px;
        height: 30px;
        color: #2196F3;
        border: 2px solid;
        border-radius: 30px;
        text-align: center;
    }

    table.table td a.view i {
        font-size: 22px;
        margin: 2px 0 0 1px;
    }

    table.table .avatar {
        border-radius: 50%;
        vertical-align: middle;
        margin-right: 10px;
    }

    table {
        counter-reset: section;
    }

    .count:before {
        counter-increment: section;
        content: counter(section);
    }
</style>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>