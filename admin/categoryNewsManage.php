<div class="container-fluid" style="margin-top:98px">

    <div class="row">
        <div class="col-lg-12">
            <button class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#newNews"><i
                    class="fa fa-plus"></i> Tạo danh mục</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="card col-lg-12">
            <div class="card-body">
                <table class="table-striped table-bordered col-md-12 text-center">
                    <thead style="background-color: rgb(111 202 203);">
                        <tr>
                            <th>Mã loại</th>
                            <th>Tên loại tin</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM CategoryNews";
                        $result = mysqli_query($conn, $sql);

                        while ($row = mysqli_fetch_assoc($result)) {
                            $Id = $row['CategoryID'];
                            $title = $row['CategoryName'];
                            echo '<tr>
                                    <td>' . $Id . '</td>
                                    <td>' . $title . '</td>
                                    <td class="text-center" style="width:95px">
                                        <div class="row mx-auto" style="width:95px">
                                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#editUser' . $Id . '" type="button">Sửa</button>';
                            if ($Id == 1) {
                                echo '<button class="btn btn-sm btn-danger" disabled style="margin-left:9px;">Xóa</button>';
                            } else {
                                echo '<form action="partials/_cateNewsManage.php" method="POST">
                                                        <button name="removeUser" class="btn btn-sm btn-danger" style="margin-left:9px;">Xóa</button>
                                                        <input type="hidden" name="Id" value="' . $Id . '">
                                                    </form>';
                            }

                            echo '</div>
                                    </td>
                                </tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Create news Modal -->
<div class="modal fade" id="newNews" tabindex="-1" role="dialog" aria-labelledby="newNews" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(111 202 203);">
                <h5 class="modal-title" id="newNews">Tạo danh mục</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="partials/_cateNewsManage.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <b><label for="title">Tên danh mục</label></b>
                        <input class="form-control" id="title" name="title" placeholder="Tên danh mục" type="text"
                            required maxlength="46">
                    </div>
                    <button type="submit" name="createNews" class="btn btn-success">Tạo</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$usersql = "SELECT * FROM `CategoryNews`";
$userResult = mysqli_query($conn, $usersql);
while ($userRow = mysqli_fetch_assoc($userResult)) {
    $Id = $userRow['CategoryID'];
    $name = $userRow['CategoryName'];
?>
<!-- editUser Modal -->
<div class="modal fade" id="editUser<?php echo $Id; ?>" tabindex="-1" role="dialog"
    aria-labelledby="editUser<?php echo $Id; ?>" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(111 202 203);">
                <h5 class="modal-title" id="editUser<?php echo $Id; ?>">Id: <b><?php echo $Id; ?></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="partials/_cateNewsManage.php" method="post">
                    <div class="form-group">
                        <b><label for="CategoryName">Tên danh mục:</label></b>
                        <input class="form-control" id="CategoryName" name="CategoryName" value="<?php echo $name; ?>"
                            type="text">
                    </div>



                    <input type="hidden" id="CategoryID" name="CategoryID" value="<?php echo $Id; ?>">
                    <button type="submit" name="editUser" class="btn btn-success">Cập nhật</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#content').summernote({
        height: 300, // Độ cao của trình soạn thảo
        placeholder: 'Nhập nội dung ở đây...',
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview']],
        ],
    });
});
</script>

<?php
}
?>