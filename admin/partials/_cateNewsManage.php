<?php
include '_dbconnect.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['removeUser'])) {
        $Id = $_POST["Id"];
        $sql = "DELETE FROM `CategoryNews` WHERE `CategoryID`='$Id'";
        $result = mysqli_query($conn, $sql);
        echo "<script>alert('Đã xóa');
            window.location=document.referrer;
            </script>";
    }

    if (isset($_POST['createNews'])) {
        $title = $_POST['title'];
        $sql = "INSERT INTO CategoryNews (CategoryName) VALUES ('$title')";
        $result = mysqli_query($conn, $sql);
        if ($result) {
            echo "<script>alert('Thêm thành công');
                            window.location=document.referrer;
                        </script>";
        } else {
            echo "<script>alert('Xảy ra lỗi');
                            window.location=document.referrer;
                        </script>";
        }
    }

    if (isset($_POST['editUser'])) {
        $id = $_POST["CategoryID"];
        $firstName = $_POST["CategoryName"];
        $sql = "UPDATE `CategoryNews` SET `CategoryName`='$firstName' WHERE `CategoryID`='$id'";
        $result = mysqli_query($conn, $sql);
        if ($result) {
            echo "<script>alert('Success');
                window.location=document.referrer;
                </script>";
        } else {
            echo "<script>alert('failed');
                window.location=document.referrer;
                </script>";
        }
    }

    if (isset($_POST['updateProfilePhoto'])) {
        $id = $_POST["newsId"];
        $check = getimagesize($_FILES["userimage"]["tmp_name"]);
        if ($check !== false) {
            $newfilename = "news-" . $id . ".jpg";

            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/lotte/img/';
            $uploadfile = $uploaddir . $newfilename;

            if (move_uploaded_file($_FILES['userimage']['tmp_name'], $uploadfile)) {
                echo "<script>alert('success');
                        window.location=document.referrer;
                    </script>";
            } else {
                echo "<script>alert('failed');
                        window.location=document.referrer;
                    </script>";
            }
        } else {
            echo '<script>alert("Please select an image file to upload.");
            window.location=document.referrer;
                </script>';
        }
    }

    if (isset($_POST['removeProfilePhoto'])) {
        $id = $_POST["newsId"];
        $filename = $_SERVER['DOCUMENT_ROOT'] . "/lotte/img/news-" . $id . ".jpg";
        if (file_exists($filename)) {
            unlink($filename);
            echo "<script>alert('Removed');
                window.location=document.referrer;
            </script>";
        } else {
            echo "<script>alert('no photo available.');
                window.location=document.referrer;
            </script>";
        }
    }
}
