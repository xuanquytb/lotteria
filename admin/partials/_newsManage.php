<?php
include '_dbconnect.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['removeNews'])) {
        $Id = $_POST["Id"];
        $sql = "DELETE FROM `news` WHERE `NewsID`='$Id'";
        $result = mysqli_query($conn, $sql);
        echo "<script>alert('Đã xóa');
            window.location=document.referrer;
            </script>";
    }

    if (isset($_POST['createNews'])) {
        $title = $_POST['title'];
        $Content = $_POST['content'];
        $CategoryID = $_POST['category'];

        if (!isset($_FILES["imageThum"])) {
            echo "Dữ liệu không đúng cấu trúc";
            die;
        }

        $sqlCount = "SELECT COUNT(*) AS total_records FROM News";
        $count = $conn->query($sqlCount);
        if ($count->num_rows > 0) {
            $row = $count->fetch_assoc();
            $idNews = $row["total_records"] + 1;
        } else {
            $idNews =  1;
        }
        $newfilename = "news-" . $idNews . ".jpg";
        $uploadDirectory =  $_SERVER['DOCUMENT_ROOT'] . '/lotte/img/';
        $uploadfile =  $uploadDirectory  . $newfilename;

        // Đường dẫn đầy đủ của hình ảnh sau khi tải lên
        $uploadPath = $uploadDirectory . basename($_FILES["imageThum"]["name"]);

        // Kiểm tra nếu file là hình ảnh
        $imageFileType = strtolower(pathinfo($uploadPath, PATHINFO_EXTENSION));
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png" && $imageFileType != "gif") {
            echo "<script>alert('Chỉ chấp nhận file hình ảnh (jpg, jpeg, png, gif).');
                    window.location=document.referrer;
                </script>";
            exit;
        }

        // Di chuyển file tải lên đến thư mục lưu trữ
        if (move_uploaded_file($_FILES["imageThum"]["tmp_name"], $uploadfile)) {
            $sql = "INSERT INTO News (Title, Content, CategoryID, PublishDate , ImageURL) VALUES ('$title', '$Content',$CategoryID, NOW(), '$newfilename')";
            $result = mysqli_query($conn, $sql);
            if ($result) {
                echo "<script>alert('Thêm thành công');
                            window.location=document.referrer;
                        </script>";
            } else {
                echo "<script>alert('Xảy ra lỗi');
                            window.location=document.referrer;
                        </script>";
            }
        } else {
            echo "Có lỗi khi tải lên hình ảnh.";
        }
    }

    if (isset($_POST['editUser'])) {
        $id = $_POST["id"];
        $title = $_POST["title"];
        $content = $_POST["content"];

        if (!isset($_FILES["imageThum"])) {
            echo "Dữ liệu không đúng cấu trúc";
            die;
        }
        $newfilename = "news-" . $id . ".jpg";
        $uploadDirectory =  $_SERVER['DOCUMENT_ROOT'] . '/lotte/img/';
        $uploadfile =  $uploadDirectory  . $newfilename;

        // Đường dẫn đầy đủ của hình ảnh sau khi tải lên
        $uploadPath = $uploadDirectory . basename($_FILES["imageThum"]["name"]);

        // Kiểm tra nếu file là hình ảnh
        $imageFileType = strtolower(pathinfo($uploadPath, PATHINFO_EXTENSION));
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png" && $imageFileType != "gif") {
            echo "<script>alert('Chỉ chấp nhận file hình ảnh (jpg, jpeg, png, gif).');
                    window.location=document.referrer;
                </script>";
            exit;
        }

        // Di chuyển file tải lên đến thư mục lưu trữ
        if (move_uploaded_file($_FILES["imageThum"]["tmp_name"], $uploadfile)) {
            $sql = "UPDATE `News` SET `Title`='$title', `Content`='$content'  WHERE `NewsID`='$id'";
            $result = mysqli_query($conn, $sql);
            if ($result) {
                echo "<script>alert('Success');
                window.location=document.referrer;
                </script>";
            } else {
                echo "<script>alert('failed');
                window.location=document.referrer;
                </script>";
            }
        } else {
            echo "Có lỗi khi tải lên hình ảnh.";
        }
    }


    if (isset($_POST['updateProfilePhoto'])) {
        $id = $_POST["newsId"];
        $check = getimagesize($_FILES["userimage"]["tmp_name"]);
        if ($check !== false) {
            $newfilename = "news-" . $id . ".jpg";

            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/lotte/img/';
            $uploadfile = $uploaddir . $newfilename;

            if (move_uploaded_file($_FILES['userimage']['tmp_name'], $uploadfile)) {
                echo "<script>alert('success');
                        window.location=document.referrer;
                    </script>";
            } else {
                echo "<script>alert('failed');
                        window.location=document.referrer;
                    </script>";
            }
        } else {
            echo '<script>alert("Please select an image file to upload.");
            window.location=document.referrer;
                </script>';
        }
    }

    if (isset($_POST['removeProfilePhoto'])) {
        $id = $_POST["newsId"];
        $filename = $_SERVER['DOCUMENT_ROOT'] . "/lotte/img/news-" . $id . ".jpg";
        if (file_exists($filename)) {
            unlink($filename);
            echo "<script>alert('Removed');
                window.location=document.referrer;
            </script>";
        } else {
            echo "<script>alert('no photo available.');
                window.location=document.referrer;
            </script>";
        }
    }
}
