    <header class="header body-pd" id="header">
        <div class="header__toggle">
            <i class='bx bx-menu' id="header-toggle"></i>
        </div>

        <div class="header__img">
            <img class="rounded-circle" width="20px" src="http://localhost/lotte/assets/img/logo/lotteria.png" alt="">

        </div>
    </header>

    <div class="l-navbar showa" id="nav-bar">
        <nav class="nav">
            <div>
                <a href="index.php" class="nav__logo">
                    <span class="nav__logo-name">
                        <img class="rounded-circle" width="20px"
                            src="http://localhost/lotte/assets/img/logo/lotteria.png" alt="">
                        lotteria</span>
                </a>


                <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">
                            <i class="fas fa-fw fa-tachometer-alt"></i>
                            <span>Dashboard</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                            aria-expanded="true" aria-controls="collapseTwo">
                            <img src="http://localhost/lotte/assets/svg/user.svg" alt="">
                            <span>Quản lý tài khoản</span>
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                            data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <h6 class="collapse-header">Quản lý tài khoản:</h6>
                                <a class="collapse-item" href="index.php?page=userManage">Quản lý tài khoản</a>
                                <a class="collapse-item" href="index.php?page=cusManage">Quản lý khách hàng</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCate"
                            aria-expanded="true" aria-controls="collapseCate">
                            <img src="http://localhost/lotte/assets/svg/category.svg" alt="">
                            <span>Quản lý danh mục</span>
                        </a>
                        <div id="collapseCate" class="collapse" aria-labelledby="headingTwo"
                            data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <i class='bx bx-category-alt'></i>
                                <h6 class="collapse-header">Quản lý danh mục:</h6>
                                <a class="collapse-item" href="index.php?page=categoryManage">Danh mục món ăn</a>
                                <a class="collapse-item" href="index.php?page=categoryNewsManage">Danh mục tin tức</a>
                            </div>
                        </div>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" href="index.php?page=menuManage">
                            <img src="http://localhost/lotte/assets/svg/product.svg" alt="">
                            <span>Quản lý sản phẩm</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php?page=orderManage">
                            <img src="http://localhost/lotte/assets/svg/cartv2.svg" alt="">
                            <span>Quản lý đơn hàng</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php?page=evenManage">
                            <img src="http://localhost/lotte/assets/svg/even.svg" alt="">
                            <span>Quản lý CT khuyến mại</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php?page=newsManage">
                            <img src="http://localhost/lotte/assets/svg/news.svg" alt="">
                            <span>Quản lý tin tức</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php?page=contactManage">
                            <i class="fas fa-hands-helping"></i>
                            <span>Thông tin liên hệ</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php?page=siteManage">
                            <i class="fas fa-cogs"></i>
                            <span>Cấu hình hệ thống</span></a>
                    </li>
                </ul>
            </div>
            <a href="partials/_logout.php" class="nav__link">
                <i class='bx bx-log-out nav__icon'></i>
                <span class="nav__name"></span>
            </a>
        </nav>
    </div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
<?php $page = isset($_GET['page']) ? $_GET['page'] : 'home'; ?>
$('.nav-<?php echo $page; ?>').addClass('active')
    </script>