<?php

use function PHPSTORM_META\type;

include '_dbconnect.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['removeItem'])) {
        $Id = $_POST["Id"];
        $sql = "DELETE FROM `PromotionEvents` WHERE `id`='$Id'";
        $eventNameOne = "DROP EVENT IF EXISTS one_discount_event_$Id";
        $eventNameOneEnd = "DROP EVENT IF EXISTS one_end_discount_event_$Id";
        $eventName = "DROP EVENT IF EXISTS discount_event_$Id";
        $eventNameEnd = "DROP EVENT IF EXISTS end_discount_event_$Id";

        $conn->query($eventNameOne);
        $conn->query($eventNameOneEnd);
        $conn->query($eventName);
        $conn->query($eventNameEnd);
        $result = mysqli_query($conn, $sql);
        echo "<script>alert('Đã xóa');
            window.location=document.referrer;
            </script>";
    }

    if (isset($_POST['createEven'])) {
        $name = $_POST["name"];
        $start_date = $_POST["start_date"];
        $description = $_POST["description"];
        $discount_percentage = $_POST["discount_percentage"];
        $type_even = $_POST["event_type"];
        $checkbox = $_POST["applyToMenu"];




        $dateTimeObject = date_create_from_format("Y-m-d\TH:i", $start_date);
        $datePart = $dateTimeObject->format("Y-m-d");
        $timePart = $dateTimeObject->format("H:i:s");
        $removedHyphenStringStart = strtr($datePart, array('-' => ''));
        $removedHyphenStringStartTime = strtr($timePart, array(':' => ''));


        if ($checkbox === "0") {

            if ($type_even === "loai1") {
                $sqlInsert = "INSERT INTO PromotionEvents (name, start_date, end_date, description, discount_percentage, is_active) VALUES ('$name', '$start_date',  NULL, '$type_even', $discount_percentage, true)";
                $conn->query($sqlInsert);
                $last_inserted_id = $conn->insert_id;
                $sqlEvent = "
                CREATE EVENT one_discount_event_$last_inserted_id
                    ON SCHEDULE AT TIMESTAMP('$datePart $timePart')
                    ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = $discount_percentage;
                    END;
                ";
                $sqlEventEnd = "
                CREATE EVENT one_end_discount_event_$last_inserted_id
                    ON SCHEDULE AT TIMESTAMP('$datePart 23:59:00')
                    ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = 5;
                    END;
                ";


                if ($conn->query($sqlEvent) && $conn->query($sqlEventEnd)) {
                    echo "<script>alert('Đã lên lịch cho sự kiện');
                window.location=document.referrer;
                </script>";
                } else {
                    echo "Error adding event: " . $conn->error;
                }
            } elseif ($type_even === "loai2") {
                $end_date = $_POST["end_date"];
                $dateTimeObject = date_create_from_format("Y-m-d\TH:i", $end_date);
                $datePartend = $dateTimeObject->format("Y-m-d");
                $timePartend = $dateTimeObject->format("H:i:s");

                $removedHyphenStringEnd = strtr($datePartend, array('-' => ''));
                $removedHyphenStringEndTime = strtr($timePartend, array(':' => ''));

                $sqlInsert = "INSERT INTO PromotionEvents (name, start_date, end_date, description, discount_percentage, is_active) VALUES ('$name', '$start_date', '$end_date', '$type_even', $discount_percentage, true)";
                $conn->query($sqlInsert);
                $last_inserted_id = $conn->insert_id;



                $sqlEvent = "
                CREATE EVENT discount_event_$last_inserted_id
                ON SCHEDULE AT TIMESTAMP('$datePart $timePart')
                ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = $discount_percentage;
                    END;
                ";
                $sqlEventEnd = "
                CREATE EVENT end_discount_event_$last_inserted_id
                    ON SCHEDULE AT TIMESTAMP('$datePartend $timePartend')
                    ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = 5;
                    END;
                ";


                if ($conn->query($sqlEvent) && $conn->query($sqlEventEnd)) {
                    echo "<script>alert('Đã lên lịch cho sự kiện');
                window.location=document.referrer;
                </script>";
                } else {
                    echo "Error adding event: " . $conn->error;
                }
            }
        } else {
            if ($type_even === "loai1") {
                $sqlInsert = "INSERT INTO PromotionEvents (name, start_date, end_date, description, discount_percentage, is_active) VALUES ('$name', '$start_date',  NULL, '$type_even', $discount_percentage, true)";
                $conn->query($sqlInsert);
                $last_inserted_id = $conn->insert_id;
                $idCate = $_POST["categoryId"];
                $sqlEvent = "
                CREATE EVENT one_discount_event_$last_inserted_id
                ON SCHEDULE AT TIMESTAMP('$datePart $timePart')
                ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = $discount_percentage where foodCategorieId = $idCate;
                    END;
                ";

                $sqlEventEnd = "
                CREATE EVENT one_end_discount_event_$last_inserted_id
                    ON SCHEDULE AT TIMESTAMP('$datePart 59:59:00')
                    ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = 5;
                    END;
                ";


                if ($conn->query($sqlEvent) && $conn->query($sqlEventEnd)) {
                    echo "<script>alert('Đã lên lịch cho sự kiện');
                window.location=document.referrer;
                </script>";
                } else {
                    echo "Error adding event: " . $conn->error;
                }
            } elseif ($type_even === "loai2") {
                $idCate = $_POST["categoryId"];
                $end_date = $_POST["end_date"];
                $sqlInsert = "INSERT INTO PromotionEvents (name, start_date, end_date, description, discount_percentage, is_active) VALUES ('$name', '$start_date', '$end_date', '$type_even', $discount_percentage, true)";
                $conn->query($sqlInsert);
                $last_inserted_id = $conn->insert_id;
                $dateTimeObject = date_create_from_format("Y-m-d\TH:i", $end_date);
                $datePartend = $dateTimeObject->format("Y-m-d");
                $timePartend = $dateTimeObject->format("H:i:s");

                $removedHyphenStringEnd = strtr($datePartend, array('-' => ''));
                $removedHyphenStringEndTime = strtr($timePartend, array(':' => ''));

                $sqlEvent = "
                CREATE EVENT discount_event_$last_inserted_id
                ON SCHEDULE AT TIMESTAMP('$datePart $timePart')
                ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = $discount_percentage where foodCategorieId = $idCate;
                    END;
                ";
                $sqlEventEnd = "
                CREATE EVENT end_discount_event_$last_inserted_id
                    ON SCHEDULE AT TIMESTAMP('$datePartend $timePartend')
                    ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = 5 where foodCategorieId = $idCate;
                    END;
                ";


                if ($conn->query($sqlEvent) && $conn->query($sqlEventEnd)) {
                    echo "<script>alert('Đã lên lịch cho sự kiện');
                window.location=document.referrer;
                </script>";
                } else {
                    echo "Error adding event: " . $conn->error;
                }
            }
        }
    }

    if (isset($_POST['updateEven'])) {
        $id = $_POST["id"];
        $name = $_POST["nameUpdate"];
        $start_date = $_POST["start_date_update"];
        $discount_percentage = $_POST["discount_percentage_update"];
        $type_even = $_POST["typeSelect"];

        echo $type_even;


        $dateTimeObject = date_create_from_format("Y-m-d\TH:i", $start_date);
        $datePart = $dateTimeObject->format("Y-m-d");
        $timePart = $dateTimeObject->format("H:i:s");
        $removedHyphenStringStart = strtr($datePart, array('-' => ''));
        $removedHyphenStringStartTime = strtr($timePart, array(':' => ''));




        if ($type_even === "loai1") {
            $sqlInsert = "UPDATE `PromotionEvents` SET `name`='$name', `start_date`='$start_date', `discount_percentage`='$discount_percentage'  WHERE `id`='$id'";

            $conn->query($sqlInsert);
            $last_inserted_id = $conn->insert_id;
            $sqlEvent = "
                ALTER EVENT one_discount_event_$id
                    ON SCHEDULE AT TIMESTAMP('$datePart $timePart')
                    ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = $discount_percentage;
                    END;
                ";
            $sqlEventEnd = "
                ALTER EVENT one_end_discount_event_$id
                    ON SCHEDULE AT TIMESTAMP('$datePart 23:59:00')
                    ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = 5;
                    END;
                ";

            if ($conn->query($sqlEvent) && $conn->query($sqlEventEnd)) {
                echo "<script>alert('Đã lên lịch cho sự kiện');
                window.location=document.referrer;
                </script>";
            } else {
                echo "Error adding event: " . $conn->error;
            }
        } elseif ($type_even === "loai2") {
            $end_date = $_POST["end_date_update"];
            $dateTimeObject = date_create_from_format("Y-m-d\TH:i", $end_date);
            $datePartend = $dateTimeObject->format("Y-m-d");
            $timePartend = $dateTimeObject->format("H:i:s");

            $removedHyphenStringEnd = strtr($datePartend, array('-' => ''));
            $removedHyphenStringEndTime = strtr($timePartend, array(':' => ''));

            $sqlInsert = "UPDATE `PromotionEvents` SET `name`='$name', `start_date`='$start_date', `end_date`='$end_date',`discount_percentage`='$discount_percentage'  WHERE `id`='$id'";
            $conn->query($sqlInsert);
            $last_inserted_id = $conn->insert_id;



            $sqlEvent = "
                ALTER EVENT discount_event_$id
                ON SCHEDULE AT TIMESTAMP('$datePart $timePart')
                ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = $discount_percentage;
                    END;
                ";
            $sqlEventEnd = "
                ALTER EVENT end_discount_event_$id
                    ON SCHEDULE AT TIMESTAMP('$datePartend $timePartend')
                    ON COMPLETION PRESERVE
                    DO
                    BEGIN
                        UPDATE `opd`.`food` SET `discount` = 5;
                    END;
                ";

            if ($conn->query($sqlEvent) && $conn->query($sqlEventEnd)) {
                echo "<script>alert('Đã lên lịch cho sự kiện');
                window.location=document.referrer;
                </script>";
            } else {
                echo "Error adding event: " . $conn->error;
            }
        }
    }
}
