<?php
include '_dbconnect.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['createItem'])) {
        $name = $_POST["name"];
        $description = $_POST["description"];
        $categoryId = $_POST["categoryId"];
        $price = $_POST["price"];
        $discount = $_POST["discount"];

        // $sql = "INSERT INTO `food` (`foodName`, `foodPrice`, `foodDesc`, `foodCategorieId`, `foodPubDate`, `discount`) VALUES ('$name', '$price', '$description', '$categoryId', current_timestamp() , $discount)";
        $sql = "CALL `opd`.`InsertFoodAndPrice`('$name', '$price', $discount, '$description', '$categoryId')";
        $result = mysqli_query($conn, $sql);

        $foodId = $conn->insert_id;
        if ($result) {
            $check = getimagesize($_FILES["imageup"]["tmp_name"]);
            if ($check !== false) {

                $newName = 'food-' . $foodId;
                $newfilename = $newName . ".jpg";

                $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/lotte/img/';
                $uploadfile = $uploaddir . $newfilename;

                if (move_uploaded_file($_FILES['imageup']['tmp_name'], $uploadfile)) {
                    echo "<script>alert('success');
                        window.location=document.referrer;
                    </script>";
                } else {
                    echo "<script>alert('failed');
                        window.location=document.referrer;
                    </script>";
                }
            } else {
                echo '<script>alert("Please select an image file to upload.");
                        window.location=document.referrer;
                    </script>';
            }
        } else {
            echo "<script>alert('failed');
                    window.location=document.referrer;
                </script>";
        }
    }
    if (isset($_POST['removeItem'])) {
        $foodId = $_POST["foodId"];
        $sql = "DELETE FROM `food` WHERE `foodId`='$foodId'";
        $result = mysqli_query($conn, $sql);
        $filename = $_SERVER['DOCUMENT_ROOT'] . "/lotte/img/food-" . $foodId . ".jpg";
        if ($result) {
            if (file_exists($filename)) {
                unlink($filename);
            }
            echo "<script>alert('Removed');
                window.location=document.referrer;
            </script>";
        } else {
            echo "<script>alert('failed');
            window.location=document.referrer;
            </script>";
        }
    }
    if (isset($_POST['updateItem'])) {
        $foodId = $_POST["foodId"];
        $foodName = $_POST["name"];
        $foodDesc = $_POST["desc"];
        $foodPrice = $_POST["price"];
        $discount = $_POST["discount"];
        $foodCategorieId = $_POST["catId"];

        // $sql = "UPDATE `food` SET `foodName`='$foodName', `foodPrice`='$foodPrice', `foodDesc`='$foodDesc', `foodCategorieId`='$foodCategorieId' , `discount`=$discount WHERE `foodId`='$foodId'";
        $sql = "CALL `opd`.`UpdateFoodAndPrice`('$foodId', '$foodName','$foodPrice','$discount','$foodDesc','$foodCategorieId');";


        $result = mysqli_query($conn, $sql);
        if ($result) {
            echo "<script>alert('update');
                window.location=document.referrer;
                </script>";
        } else {
            echo "<script>alert('failed');
                window.location=document.referrer;
                </script>";
        }
    }
    if (isset($_POST['updateItemPhoto'])) {
        $foodId = $_POST["foodId"];
        $check = getimagesize($_FILES["itemimage"]["tmp_name"]);
        if ($check !== false) {
            $newName = 'food-' . $foodId;
            $newfilename = $newName . ".jpg";

            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/lotte/img/';
            $uploadfile = $uploaddir . $newfilename;

            if (move_uploaded_file($_FILES['itemimage']['tmp_name'], $uploadfile)) {
                echo "<script>alert('success');
                        window.location=document.referrer;
                    </script>";
            } else {
                echo "<script>alert('failed');
                        window.location=document.referrer;
                    </script>";
            }
        } else {
            echo '<script>alert("Please select an image file to upload.");
            window.location=document.referrer;
                </script>';
        }
    }
}
