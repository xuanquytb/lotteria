<?php
$itemModalSql = "SELECT * FROM `orders`";
$itemModalResult = mysqli_query($conn, $itemModalSql);
while ($itemModalRow = mysqli_fetch_assoc($itemModalResult)) {
    $orderid = $itemModalRow['orderId'];
    $userid = $itemModalRow['userId'];
    $orderStatus = $itemModalRow['orderStatus'];
?>

<!-- Modal -->
<div class="modal fade" id="orderStatus<?php echo $orderid; ?>" tabindex="-1" role="dialog"
    aria-labelledby="orderStatus<?php echo $orderid; ?>" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(111 202 203);">
                <h5 class="modal-title" id="orderStatus<?php echo $orderid; ?>">Chi tiết đơn hàng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="partials/_orderManage.php" method="post" style="border-bottom: 2px solid #dee2e6;">
                    <div class="text-left my-2">
                        <b><label for="name">Trạng thái</label></b>
                        <div class="row mx-2">

                            <div style="display: flex;gap: 20px;">
                                <select name="status" id="status" class="custom-select browser-default">
                                    <option value="1" <?php echo ($orderStatus == 1) ? 'selected' : ''; ?>>Chờ xác nhận
                                    </option>
                                    <option value="2" <?php echo ($orderStatus == 2) ? 'selected' : ''; ?>>Đã xác nhận
                                    </option>
                                    <option value="3" <?php echo ($orderStatus == 3) ? 'selected' : ''; ?>>Đang chuẩn bị
                                    </option>
                                    <option value="4" <?php echo ($orderStatus == 4) ? 'selected' : ''; ?>>Đang giao
                                        hàng</option>
                                    <option value="5" <?php echo ($orderStatus == 5) ? 'selected' : ''; ?>>Đã giao hàng
                                    </option>
                                    <option value="6" <?php echo ($orderStatus == 6) ? 'selected' : ''; ?>>Không hợp lệ
                                    </option>
                                    <option value="7" <?php echo ($orderStatus == 7) ? 'selected' : ''; ?>>Đã bị hủy
                                    </option>
                                </select>

                            </div>
                            <button type="button" class="btn btn-secondary ml-1" data-container="body"
                                data-toggle="popover" title="Mã trạng thái" data-placement="bottom" data-html="true"
                                data-content="0=Chờ xác nhận.<br> 1=Đã xác nhận.<br> 2=Đang chuẩn bị.<br> 3=Đang trên đường giao đến bạn!<br> 4=Đã giao hàng.<br> 5=Không hợp lệ.<br> 6=Đã bị hủy.">
                                <i class="fas fa-info"></i>
                            </button>
                        </div>
                    </div>
                    <input type="hidden" id="orderId" name="orderId" value="<?php echo $orderid; ?>">
                    <button type="submit" class="btn btn-success mb-2" name="updateStatus">Cập nhật</button>
                </form>
                <?php
                    $deliveryDetailSql = "SELECT * FROM `deliverydetails` WHERE `orderId`= $orderid";
                    $deliveryDetailResult = mysqli_query($conn, $deliveryDetailSql);
                    $deliveryDetailRow = mysqli_fetch_assoc($deliveryDetailResult);
                    if ($deliveryDetailRow) {
                        $trackId = $deliveryDetailRow['id'];
                        $deliveryBoyName = $deliveryDetailRow['deliveryBoyName'];
                        $deliveryBoyPhoneNo = $deliveryDetailRow['deliveryBoyPhoneNo'];
                        $deliveryTime = $deliveryDetailRow['deliveryTime'];
                    }
                    if ($orderStatus > 1 && $orderStatus < 5) {
                    ?>
                <form action="partials/_orderManage.php" method="post">
                    <div class="text-left my-2">
                        <b><label for="name">Người giao hàng</label></b>
                        <input class="form-control" id="name" name="name" value="<?php echo $deliveryBoyName; ?>"
                            type="text" required>
                    </div>
                    <div class="text-left my-2 row">
                        <div class="form-group col-md-6">
                            <b><label for="phone">Số điện thoại</label></b>
                            <input class="form-control" maxlength="10" id="phone" name="phone"
                                value="<?php echo $deliveryBoyPhoneNo; ?>" type="tel" required pattern="[0-9]{10}">
                        </div>
                        <div class="form-group col-md-6">
                            <b><label for="catId">Thời gian giao hàng</label></b>
                            <input class="form-control" id="time" name="time" value="<?php echo $deliveryTime; ?>"
                                type="number" min="1" max="120" required>
                        </div>
                    </div>
                    <input type="hidden" id="trackId" name="trackId" value="<?php echo $trackId; ?>">
                    <input type="hidden" id="orderId" name="orderId" value="<?php echo $orderid; ?>">
                    <button type="submit" class="btn btn-success" name="updateDeliveryDetails">Cập nhật</button>
                </form>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php
}
?>

<style>
.popover {
    top: -77px !important;
}
</style>

<script>
$(function() {
    $('[data-toggle="popover"]').popover();
});
</script>