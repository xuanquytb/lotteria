<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <title id="title">Lịch sử chỉnh sửa giá</title>
    <link rel="icon" href="img/logo.jpg" type="image/x-icon">

</head>

<body>
    <?php include 'partials/_dbconnect.php'; ?>
    <h5>Lịch sử chỉnh sửa giá sản phẩm</h5>
    <form action="">
        <table class="table table-striped table-hover text-center " id="NoOrder">
            <thead style="background-color: rgb(111 202 203);">
                <tr>
                    <th>Mã món</th>
                    <th>Tên món</th>
                    <th>Giá bán</th>
                    <th>Ngày sửa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $product_id = isset($_GET['idProduct']) ? $_GET['idProduct'] : 1;
                $sql = "SELECT  * FROM food  JOIN foodprices ON food.foodId = foodprices.idFood where idFood = $product_id";
                $result = mysqli_query($conn, $sql);

                while ($row = mysqli_fetch_assoc($result)) {
                    $Id = $row['foodId'];
                    $foodName = $row['foodName'];
                    $foodPrice = $row['price'];
                    $updateAt = $row['updateAt'];
                    echo '<tr>
                                <td>' . $Id . '</td>
                                <td>' . $foodName . '</td>
                                
                                <td>' . $foodPrice . '</td>
                                <td>' . $updateAt . '</td>
                            </tr>';
                }

                ?>
            </tbody>
        </table>
    </form>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>