<div class="container-fluid" style="margin-top:98px">

    <div class="col-lg-12">
        <div class="row">
            <!-- FORM Panel -->

            <!-- FORM Panel -->

            <!-- Table Panel -->
            <div>
                <div class="mb-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCapNhat">Thêm
                        sản phẩm</button>

                    <!-- Modal -->
                    <div class="modal" id="modalCapNhat">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <!-- Modal body (Nội dung) -->
                                <div class="modal-body">
                                    <div class="">
                                        <form action="partials/_menuManage.php" method="post" enctype="multipart/form-data">
                                            <div class="card mb-3">
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label class="control-label">Tên món ăn: </label>
                                                        <input type="text" class="form-control" name="name" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Mô tả: </label>
                                                        <textarea cols="30" rows="3" class="form-control" name="description" required></textarea>
                                                    </div>
                                                    <div class="text-left my-2 row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Giá</label>
                                                            <input type="number" class="form-control" name="price" required min="1">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Giảm giá (%)</label>
                                                            <input type="number" class="form-control" name="discount" required min="1">
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Danh mục: </label>
                                                            <select name="categoryId" id="categoryId" class="custom-select browser-default" required>
                                                                <option hidden disabled selected value>Chọn danh mục
                                                                </option>
                                                                <?php
                                                                $catsql = "SELECT * FROM `categories`";
                                                                $catresult = mysqli_query($conn, $catsql);
                                                                while ($row = mysqli_fetch_assoc($catresult)) {
                                                                    $catId = $row['categorieId'];
                                                                    $catName = $row['categorieName'];
                                                                    echo '<option value="' . $catId . '">' . $catName . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="imageup" class="control-label">Hình ảnh</label>
                                                        <input type="file" name="imageup" id="imageup" accept=".jpg" class="form-control" required style="border:none;">
                                                        <small id="Info" class="form-text text-muted mx-3">Định dạng
                                                            .jpg
                                                            .png</small>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                    <div class="row">
                                                        <div style="margin-left: 15px;">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                                            <button type="submit" name="createItem" class="btn btn-primary"> Tạo
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card" style="width: 77vw;">
                        <div class="card-body">
                            <form action="" method="post">
                                <div>
                                    <div class="input-group mb-3">
                                        <div style="display: flex;gap: 20px;">
                                            <select name="categoryId" id="categoryId" class="custom-select browser-default">
                                                <option hidden disabled selected value>Chọn danh mục
                                                </option>
                                                <?php
                                                $catsql = "SELECT * FROM `categories`";
                                                $catresult = mysqli_query($conn, $catsql);
                                                while ($row = mysqli_fetch_assoc($catresult)) {
                                                    $catId = $row['categorieId'];
                                                    $catName = $row['categorieName'];
                                                    echo '<option value="' . $catId . '">' . $catName . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <input type="text" class="form-control" placeholder="Tên sản phẩm" name="nameFood" id="nameFood" aria-label="Recipient's username" aria-describedby="button-addon2">
                                        </div>
                                        <button class="btn btn-outline-secondary mx-2" type="submit">Lọc</button>
                                        <button class="btn btn-outline-secondary" type="submit" name="reset" onclick="resetFilters()">Reset</button>
                                    </div>
                                </div>
                                <table class="table table-bordered table-hover mb-0">
                                    <thead style="background-color: rgb(111 202 203);">
                                        <tr>
                                            <th class="text-center">Mã SP</th>
                                            <th class="text-center">Ảnh minh họa</th>
                                            <th class="text-center">Chi tiết món</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (isset($_POST['reset'])) {
                                            $nameFood = '';
                                            $categoryId =  '';
                                        }
                                        $nameFood = $_POST['nameFood'] ?? '';
                                        $categoryId = $_POST['categoryId'] ?? '';
                                        if (isset($_POST['currentPage'])) {
                                            $currentPage = $_POST['currentPage'];
                                        } else {
                                            $currentPage = 1;
                                        }
                                        $page = $currentPage;

                                        $limit = 5;
                                        $start = ($page - 1) * $limit;
                                        $sql = "SELECT *,MAX(foodprices.price) AS prices,MAX(foodprices.updateAt) AS latest_date FROM food JOIN foodprices ON food.foodId = foodprices.idFood join categories on food.foodCategorieId = categories.categorieId";

                                        if (!empty($categoryId)) {
                                            $sql .= " WHERE foodCategorieId = '$categoryId'";
                                        }

                                        // Thêm điều kiện lọc theo tên sản phẩm
                                        if (!empty($nameFood)) {
                                            $sql .= (empty($categoryId) ? " WHERE " : " AND ") . "foodName like '%$nameFood%'";
                                        }

                                        $sql .= " GROUP  BY idFood HAVING latest_date IS NOT NULL limit $start,$limit ";

                                        $result = mysqli_query($conn, $sql);
                                        while ($row = $result->fetch_assoc()) {
                                            $foodId = $row['foodId'];
                                            $foodName = $row['foodName'];
                                            $foodPrice = $row['prices'];
                                            $foodDesc = $row['foodDesc'];
                                            $foodCategorieId = $row['foodCategorieId'];
                                            $CategorieName = $row['categorieName'];
                                            echo '<tr>
                                        <td class="text-center">' . $foodId . '</td>
                                        <td>
                                            <img src="/lotte/img/food-' . $foodId . '.jpg" alt="image for this item" width="150px" height="150px">
                                        </td>
                                        <td>
                                            <p>Tên món : <b>' . $foodName . '</b></p>
                                            <p>Loại sản phẩm : <b>' . $CategorieName . '</b></p>
                                            <p>Giá : <a  href="http://localhost/lotte/admin/viewfoodlist.php?idProduct=' . $foodId . '"><b style="font-size: 16px;color: #142f43;border: 1px solid gray;padding: 1px 5px;border-radius: 8px;">' . $foodPrice . '</b></p>
                                        </td>
                                        <td class="text-center">
                                                        <div class="row mx-auto" style="width:112px">
                                                        <button class="btn btn-sm btn-primary" type="button" data-toggle="modal" data-target="#updateItem' . $foodId . '">Sửa</button>
                                                            <form action="partials/_menuManage.php" method="POST">
                                                                <button name="removeItem" class="btn btn-sm btn-danger" style="margin-left:9px;">Xóa</button>
                                                                <input type="hidden" name="foodId" value="' . $foodId . '">
                                                            </form>
                                                        </div>
                                        </td>
                                    </tr>';
                                        }


                                        ?>

                                    </tbody>

                                </table>


                                <ul class="pagination" style="display: flex; gap : 5px ; margin-top: 20px;">
                                    <?php
                                    $productsPerPage = 5;
                                    $sqlTotal = "SELECT COUNT(*) as total FROM food";
                                    if (!empty($categoryId)) {
                                        $sqlTotal .= " WHERE foodCategorieId = '$categoryId'";
                                    }

                                    // Thêm điều kiện lọc theo tên sản phẩm
                                    if (!empty($nameFood)) {
                                        $sqlTotal .= (empty($categoryId) ? " WHERE " : " AND ") . "foodName like '%$nameFood%'";
                                    }
                                    $totalResult = $conn->query($sqlTotal);
                                    $totalRow = $totalResult->fetch_assoc()['total'];
                                    $totalPages = ceil($totalRow / $productsPerPage);
                                    if (isset($_POST['currentPage'])) {
                                        $currentPage = $_POST['currentPage'];
                                    } else {
                                        $currentPage = 1;
                                    }
                                    $counter = 1;
                                    while ($counter <= $totalPages) {
                                        echo "<li class='page-item' >
                                                <form method='post' action=''>
                                                    <input type='hidden' name='currentPage' value='$counter'>
                                                    <button style=' border: none; width: 60px ;border-radius: 5px;' type='submit'>$counter</button>
                                                </form>
                                                </li>";
                                        $counter++;
                                    } ?>

                                </ul>

                            </form>
                        </div>

                        <script>
                            function resetFilters() {
                                document.getElementById("categoryId").selectedIndex = 0; // Reset dropdown
                                document.getElementById("nameFood").value = ''; // Reset text input
                                window.location.reload();
                            }
                        </script>

                    </div>
                </div>
            </div>

            <!-- Table Panel -->
        </div>
    </div>
</div>

<?php
$sql = "SELECT *,MAX(foodprices.price) AS prices,MAX(foodprices.updateAt) AS latest_date FROM food JOIN foodprices ON food.foodId = foodprices.idFood join categories on food.foodCategorieId = categories.categorieId GROUP  BY idFood HAVING latest_date IS NOT NULL";
$result = mysqli_query($conn, $sql);
while ($foodRow = mysqli_fetch_assoc($result)) {
    $foodId = $foodRow['foodId'];
    $foodName = $foodRow['foodName'];
    $foodPrice = $foodRow['prices'];
    $foodCategorieId = $foodRow['foodCategorieId'];
    $discount = $foodRow['discount'];
    $foodDesc = $foodRow['foodDesc'];
?>

    <!-- Modal -->
    <div class="modal fade" id="updateItem<?php echo $foodId; ?>" tabindex="-1" role="dialog" aria-labelledby="updateItem<?php echo $foodId; ?>" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: rgb(111 202 203);">
                    <h5 class="modal-title" id="updateItem<?php echo $foodId; ?>">Id món: <?php echo $foodId; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="partials/_menuManage.php" method="post" enctype="multipart/form-data">
                        <div class="text-left my-2 row" style="border-bottom: 2px solid #dee2e6;">
                            <div class="form-group col-md-8">
                                <b><label for="image">Hình ảnh</label></b>
                                <input type="file" name="itemimage" id="itemimage" accept=".jpg" class="form-control" required style="border:none;" onchange="document.getElementById('itemPhoto').src = window.URL.createObjectURL(this.files[0])">
                                <small id="Info" class="form-text text-muted mx-3">Please .jpg file upload.</small>
                                <input type="hidden" id="foodId" name="foodId" value="<?php echo $foodId; ?>">
                                <button type="submit" class="btn btn-success my-1" name="updateItemPhoto">Cập nhật
                                    hình</button>
                            </div>
                            <div class="form-group col-md-4">
                                <img src="/lotte/img/food-<?php echo $foodId; ?>.jpg" id="itemPhoto" name="itemPhoto" alt="item image" width="100" height="100">
                            </div>
                        </div>
                    </form>
                    <form action="partials/_menuManage.php" method="post">
                        <div class="text-left my-2">
                            <b><label for="name">Tên món</label></b>
                            <input class="form-control" id="name" name="name" value="<?php echo $foodName; ?>" type="text" required>
                        </div>
                        <div class="text-left my-2 row">
                            <div class="form-group col-md-4">
                                <b><label for="price">Giá</label></b>
                                <input class="form-control" id="price" name="price" value="<?php echo $foodPrice; ?>" type="number" min="1" required>
                            </div>
                            <div class="form-group col-md-4">
                                <b><label for="discount">Giảm giá (%)</label></b>
                                <input class="form-control" id="discount" name="discount" value="<?php echo $discount; ?>" type="number" min="1" required>
                            </div>
                            <div class="form-group col-md-4">
                                <b><label for="catId">Mã danh mục</label></b>
                                <input class="form-control" id="catId" name="catId" value="<?php echo $foodCategorieId; ?>" type="number" min="1" required>
                            </div>
                        </div>
                        <div class="text-left my-2">
                            <b><label for="desc">Mô tả</label></b>
                            <textarea class="form-control" id="desc" name="desc" rows="2" required minlength="6"><?php echo $foodDesc; ?></textarea>
                        </div>
                        <input type="hidden" id="foodId" name="foodId" value="<?php echo $foodId; ?>">
                        <button type="submit" class="btn btn-success" name="updateItem">Cập nhật</button>
                    </form>
                </div>
            </div>
        </div>
    </div>



<?php
}
?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Lịch sử sửa giá</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModal()">Close</button>
            </div>
        </div>
    </div>
</div>