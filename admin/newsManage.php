<div class="container-fluid" style="margin-top:98px">

    <div class="row">
        <div class="col-lg-12">
            <button class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#newNews"><i
                    class="fa fa-plus"></i> Tạo tin mới</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="card col-lg-12">
            <div class="card-body">
                <form action="" method="post">
                    <div>
                        <div class="input-group mb-3">
                            <div style="display: flex;gap: 20px;">
                                <select name="categoryId" id="categoryId" class="custom-select browser-default">
                                    <option hidden disabled selected value>Chọn danh mục
                                    </option>
                                    <?php
                                    $catsql = "SELECT * FROM `categorynews`";
                                    $catresult = mysqli_query($conn, $catsql);
                                    while ($row = mysqli_fetch_assoc($catresult)) {
                                        $catId = $row['CategoryID'];
                                        $catName = $row['CategoryName'];
                                        echo '<option value="' . $catId . '">' . $catName . '</option>';
                                    }
                                    ?>
                                </select>
                                <input type="date" class="form-control" placeholder="Tên sản phẩm" name="date"
                                    id="date">
                            </div>
                            <button class="btn btn-outline-secondary mx-2" type="submit">Lọc</button>
                            <button class="btn btn-outline-secondary" type="submit" name="reset"
                                onclick="resetFilters()">Reset</button>
                        </div>
                    </div>
                    <table class="table-striped table-bordered col-md-12 text-center">
                        <thead style="background-color: rgb(111 202 203);">
                            <tr>
                                <th>Id</th>
                                <th>Tiêu đề</th>
                                <th>Nội dung</th>
                                <th>Ngày đăng</th>
                                <th>Loại tin</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            if (isset($_POST['reset'])) {
                                $date = '';
                                $categoryId =  '';
                            }
                            $date = $_POST['date'] ?? '';
                            $categoryId = $_POST['categoryId'] ?? '';
                            if (isset($_POST['currentPage'])) {
                                $currentPage = $_POST['currentPage'];
                            } else {
                                $currentPage = 1;
                            }
                            $page = $currentPage;

                            $limit = 5;
                            $start = ($page - 1) * $limit;
                            $sql = "SELECT * FROM opd.news join categorynews on news.CategoryID = categorynews.CategoryID";

                            if (!empty($categoryId)) {
                                $sql .= " WHERE news.CategoryID = '$categoryId'";
                            }

                            // Thêm điều kiện lọc theo tên sản phẩm
                            if (!empty($date)) {
                                $sql .= (empty($categoryId) ? " WHERE " : " AND ") . "PublishDate like '$date'";
                            }

                            $sql .= " limit $start,$limit";

                            $result = mysqli_query($conn, $sql);

                            while ($row = mysqli_fetch_assoc($result)) {
                                $Id = $row['NewsID'];
                                $title = $row['Title'];
                                $Content = $row['Content'];
                                $PublishDate = $row['PublishDate'];
                                $CategoryID = $row['CategoryID'];
                                $CategoryName = $row['CategoryName'];
                                echo '<tr>
                                    <td>' . $Id . '</td>
                                    <td><img src="/lotte/img/news-' . $Id . '.jpg" alt="image for this user" onError="this.src =\'/lotte/img/profilePic.jpg\'" width="100px" height="100px"></td>
                                    <td>' . $title . '</td>
                                    <td>' . date('d-m-Y', strtotime($PublishDate)) . '</td>
                                    <td>' . $CategoryName . '</td>
                                    <td class="text-center">
                                        <div class="row mx-auto" style="width:112px">
                                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#editUser' . $Id . '" type="button">Sửa</button> 
                                        <form action="partials/_newsManage.php" method="POST">
                                            <button name="removeNews" class="btn btn-sm btn-danger" style="margin-left:9px;">Xóa</button>
                                            <input type="hidden" name="Id" value="' . $Id . '">
                                        </form> </div>
                                        </td>
                                    </tr>';
                              
                            }
                            ?>
                        </tbody>
                    </table>
                </form>

                <ul class="pagination" style="display: flex; gap : 5px ; margin-top: 20px;">
                    <?php
                    $productsPerPage = 5;
                    $sqlTotal = "SELECT COUNT(*) as total FROM news";
                    $totalResult = $conn->query($sqlTotal);
                    $totalRow = $totalResult->fetch_assoc()['total'];
                    $totalPages = ceil($totalRow / $productsPerPage);
                    if (isset($_POST['currentPage'])) {
                        $currentPage = $_POST['currentPage'];
                    } else {
                        $currentPage = 1;
                    }
                    $counter = 1;
                    while ($counter <= $totalPages) {
                        echo "<li class='page-item' >
                                                <form method='post' action=''>
                                                    <input type='hidden' name='currentPage' value='$counter'>
                                                    <button style=' border: none; width: 60px ;border-radius: 5px;' type='submit'>$counter</button>
                                                </form>
                                                </li>";
                        $counter++;
                    } ?>

                </ul>
            </div>

            <script>
            function resetFilters() {
                document.getElementById("categoryId").selectedIndex = 0; // Reset dropdown
                document.getElementById("date").value = ''; // Reset text input
                window.location.reload();
            }
            </script>
        </div>
    </div>
</div>

<!-- Create news Modal -->
<div class="modal fade" id="newNews" tabindex="-1" role="dialog" aria-labelledby="newNews" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(111 202 203);">
                <h5 class="modal-title" id="newNews">Tạo tin tức</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="partials/_newsManage.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <b><label for="title">Tiêu đề</label></b>
                        <input class="form-control" id="title" name="title" placeholder="....." type="text" required
                            maxlength="46">
                    </div>

                    <div class="form-group">
                        <b><label for="category">Loại tin:</label></b>
                        <select class="form-control" id="category" name="category" required>
                            <?php
                            $sql = "SELECT * FROM CategoryNews";
                            $result = mysqli_query($conn, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                echo "<option value='{$row['CategoryID']}'>{$row['CategoryName']}</option>";
                            }
                            ?>
                        </select>

                    </div>

                    <div class="form-group">
                        <b><label for="content">Nội dung:</label></b>
                        <textarea class="form-control" id="content" name="content"></textarea>
                    </div>

                    <div class="form-group">
                        <b><label for="imageThum">Hình ảnh:</label></b>
                        <input type="file" class="form-control-file" id="imageThum" name="imageThum" accept="image/*">
                    </div>


                    <button type="submit" name="createNews" class="btn btn-success">Tạo</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$usersql = "SELECT * FROM `News`";
$userResult = mysqli_query($conn, $usersql);
while ($userRow = mysqli_fetch_assoc($userResult)) {
    $Id = $userRow['NewsID'];
    $title = $userRow['Title'];
    $Content = $userRow['Content'];
    $CategoryID = $userRow['CategoryID'];
?>
<!-- editUser Modal -->
<div class="modal fade" id="editUser<?php echo $Id; ?>" tabindex="-1" role="dialog"
    aria-labelledby="editUser<?php echo $Id; ?>" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(111 202 203);">
                <h5 class="modal-title" id="editUser<?php echo $Id; ?>">Id: <b><?php echo $Id; ?></b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="partials/_newsManage.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <b><label for="title">Tiêu đề</label></b>
                        <input class="form-control" id="title" name="title" placeholder="....." type="text" required
                            maxlength="46" value="<?php echo $title; ?>">
                    </div>

                    <div class="form-group">
                        <b><label for="category">Loại tin:</label></b>
                        <select class="form-control" id="category" name="category" required>
                            <?php
                                $sql = "SELECT * FROM CategoryNews";
                                $result = mysqli_query($conn, $sql);
                                while ($row = mysqli_fetch_assoc($result)) {
                                    echo "<option value='{$row['CategoryID']}'>{$row['CategoryName']}</option>";
                                }
                                ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <b><label for="contentNewsV2">Nội dung:</label></b>
                        <textarea class="form-control" id="contentNewsV2" name="contentNewsV2"></textarea>
                    </div>

                    <div class="form-group">
                        <b><label for="imageThum">Hình ảnh:</label></b>
                        <input type="file" class="form-control-file" id="imageThum" name="imageThum" accept="image/*">
                    </div>

                    <input type="hidden" id="id" name="id" value="<?php echo $Id; ?>">
                    <button type="submit" name="editUser" class="btn btn-success">Tạo</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#contentNewsV2').summernote({
        height: 300, // Độ cao của trình soạn thảo
        placeholder: 'Nhập nội dung ở đây...',
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview']],
        ],
    });
});
$(document).ready(function() {
    $('#content').summernote({
        height: 300, // Độ cao của trình soạn thảo
        placeholder: 'Nhập nội dung ở đây...',
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview']],
        ],
    });
});
</script>

<?php
}
?>