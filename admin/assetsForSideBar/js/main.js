/*===== SHOW NAVBAR  =====*/
const showNavbar = (toggleId, navId, bodyId, headerId, navItem) => {
  const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId),
    bodypd = document.getElementById(bodyId),
    headerpd = document.getElementById(headerId);

  // Validate that all variables exist
  if (toggle && nav && bodypd && headerpd && navItem) {
    toggle.addEventListener("click", () => {
      // show navbar
      nav.classList.toggle("showa");
      // change icon
      toggle.classList.toggle("bx-x");
      // add padding to body
      bodypd.classList.toggle("body-pd");
      // add padding to header
      headerpd.classList.toggle("body-pd");
      navItem.classList.toggle("nav-item");
    });
  }
};

showNavbar("header-toggle", "nav-bar", "body-pd", "header", "nav-item");
