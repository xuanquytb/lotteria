<div class="container-fluid" style="">
    <div class="row">
        <div class="card col-lg-12">
            <div class="card-body">
                <div class="row">
                    <!-- Earnings (Monthly) Card Example -->
                    <?php
                    try {
                        // Chuẩn bị truy vấn SQL
                        $sql = "SELECT SUM(`orders`.`amount`) AS totalRevenue FROM `orders`";
                        $sqlDay = "SELECT SUM(`orders`.`amount`) AS totalRevenue FROM `orders` WHERE DATE(`orders`.`orderDate`) = CURDATE()";
                        $sqlCount = "SELECT count(*) as count FROM opd.orders where orderStatus = 0";
                        $sqlCountuUser = "SELECT count(*) as count FROM opd.users where userType = '0'";

                        // Thực hiện truy vấn
                        $result = $conn->query($sql);
                        $resultDay = $conn->query($sqlDay);
                        $resultCount = $conn->query($sqlCount);
                        $resultCountUser = $conn->query($sqlCountuUser);

                        // Kiểm tra và in kết quả
                        if ($result->num_rows > 0) {
                            $row = $result->fetch_assoc(); // Sử dụng fetch_assoc() thay vì fetch()
                            $totalRevenue = ($row) ? $row["totalRevenue"] : 0;
                        }
                        if ($resultDay->num_rows > 0) {
                            $rowDay = $resultDay->fetch_assoc(); // Sử dụng fetch_assoc() thay vì fetch()
                            $totalRevenueDay = ($rowDay) ? $rowDay["totalRevenue"] : 0;
                        }
                        if ($resultCount->num_rows > 0) {
                            $rowCount = $resultCount->fetch_assoc(); // Sử dụng fetch_assoc() thay vì fetch()
                            $totalRevenueCount = ($rowCount) ? $rowCount["count"] : 0;
                        }
                        if ($resultCountUser->num_rows > 0) {
                            $rowCountUser = $resultCountUser->fetch_assoc(); // Sử dụng fetch_assoc() thay vì fetch()
                            $totalRevenueCountUser = ($rowCountUser) ? $rowCountUser["count"] : 0;
                        }
                    } catch (PDOException $e) {
                        echo "Lỗi kết nối: " . $e->getMessage();
                    }
                    ?>

                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            Doanh thu</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            <?php echo number_format($totalRevenue, 0); ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                            Doanh thu ngày
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                <?php echo number_format($totalRevenueDay, 0); ?>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Số khách hàng</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            <?php echo number_format($totalRevenueCountUser, 0); ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-comments fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pending Requests Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                            Đơn hàng chờ</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            <?php echo number_format($totalRevenueCount, 0); ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-comments fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-md-6">
                        <h5>Bán chạy trong ngày</h5>
                        <table class="table table-striped table-hover text-center " id="NoOrder">
                            <thead style="background-color: rgb(111 202 203);">
                                <tr>
                                    <th>Mã món</th>
                                    <th>Tên món</th>
                                    <th>Giá bán</th>
                                    <th>Số lần đặt</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT  *, MAX(foodprices.price) AS prices, MAX(foodprices.updateAt) AS latest_date, orders.orderDate, COUNT(*) AS sold FROM opd.orderitems JOIN food ON orderitems.foodId = food.foodId JOIN orders ON orderitems.orderId = orders.orderId JOIN foodprices ON food.foodId = foodprices.idFood WHERE DATE(orders.orderDate) = CURDATE() GROUP BY food.foodId HAVING latest_date IS NOT NULL ORDER BY sold DESC;";
                                $result = mysqli_query($conn, $sql);
                                $counter = 0;
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $Id = $row['foodId'];
                                    $foodName = $row['foodName'];
                                    $foodPrice = $row['prices'];
                                    $sold = $row['sold'];
                                    $counter++;

                                    echo '<tr>
                                <td>' . $Id . '</td>
                                <td>' . $foodName . '</td>
                                
                                <td>' . $foodPrice . '</td>
                                <td>' . $sold . '</td>
                            </tr>';
                                }
                                if ($counter == 0) {
                                ?><script>
                                document.getElementById("NoOrder").innerHTML =
                                    '<div class="alert alert-info alert-dismissible fade show" role="alert" style="width:100%"> Chưa có đơn hàng!	</div>';
                                </script> <?php
                                            }
                                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h5>Món ăn bán chạy</h5>
                        <table class="table table-striped table-hover text-center " id="NoOrder">
                            <thead style="background-color: rgb(111 202 203);">
                                <tr>
                                    <th>Mã món</th>
                                    <th>Tên món</th>
                                    <th>Giá bán</th>
                                    <th>Số lần đặt</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT  *, MAX(foodprices.price) AS prices, MAX(foodprices.updateAt) AS latest_date, orders.orderDate, COUNT(*) AS sold FROM opd.orderitems JOIN food ON orderitems.foodId = food.foodId JOIN orders ON orderitems.orderId = orders.orderId JOIN foodprices ON food.foodId = foodprices.idFood  GROUP BY food.foodId HAVING latest_date IS NOT NULL ORDER BY sold DESC;";
                                $result = mysqli_query($conn, $sql);
                                $counter = 0;
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $Id = $row['foodId'];
                                    $foodName = $row['foodName'];
                                    $foodPrice = $row['prices'];
                                    $sold = $row['sold'];
                                    $counter++;

                                    echo '<tr>
                                <td>' . $Id . '</td>
                                <td>' . $foodName . '</td>
                                
                                <td>' . $foodPrice . '</td>
                                <td>' . $sold . '</td>
                            </tr>';
                                }
                                if ($counter == 0) {
                                ?><script>
                                document.getElementById("NoOrder").innerHTML =
                                    '<div class="alert alert-info alert-dismissible fade show" role="alert" style="width:100%"> Chưa có đơn hàng!	</div>';
                                </script> <?php
                                            }
                                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="row ">
                    <div class="col-md-12">
                        <h5>Đơn hàng mới</h5>
                        <table class="table table-striped table-hover text-center " id="NoOrder">
                            <thead style="background-color: rgb(111 202 203);">
                                <tr>
                                    <th>Id khách hàng</th>
                                    <th>Số điện thoại</th>
                                    <th>Địa chỉ</th>
                                    <th>Tổng thanh toán</th>
                                    <th>Trạng thái</th>
                                    <th>Đơn hàng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT * FROM `orders` ORDER BY `orderDate` DESC";
                                $result = mysqli_query($conn, $sql);
                                $counter = 0;
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $orderId = $row['orderId'];
                                    $address = $row['address'];
                                    $phoneNo = $row['phoneNo'];
                                    $amount = $row['amount'];
                                    $orderDate = $row['orderDate'];
                                    $orderStatus = $row['orderStatus'];
                                    $counter++;

                                    echo '<tr>
                                <td>' . $orderId . '</td>
                                <td>' . $phoneNo . '</td>
                                <td>' . $address . '</td>
                                <td>' . $amount . '</td>
                                <td><a href="#" data-toggle="modal" data-target="#orderStatus' . $orderId . '" class="view"><img src="../assets/svg/deli.svg" alt=""></a></td>
                                <td><a href="#" data-toggle="modal" data-target="#orderItem' . $orderId . '" class="view" title="View Details"><img src="../assets/svg/eye.svg" alt=""></a></td>
                            </tr>';
                                }
                                if ($counter == 0) {
                                ?><script>
                                document.getElementById("NoOrder").innerHTML =
                                    '<div class="alert alert-info alert-dismissible fade show" role="alert" style="width:100%"> Chưa có đơn hàng!	</div>';
                                </script> <?php
                                            }
                                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>


                <?php
                include 'partials/_orderItemModal.php';
                include 'partials/_orderStatusModal.php';
                ?>

            </div>


        </div>
    </div>
</div>
</div>