<div class="container-fluid" style="margin-top:98px">

    <div class="col-lg-12">
        <div class="row">

            <div>
                <div class="mb-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newEvens">Tạo sự
                        kiện</button>
                </div>

                <div class="card" style="width: 77vw;">
                    <div class="card-body">
                        <table class="table table-bordered table-hover mb-0">
                            <thead style="background-color: rgb(111 202 203);">
                                <tr>
                                    <th width="100px">Mã sự kiện</th>
                                    <th width="350px">Tên sự kiện</th>
                                    <th>Ngày diển ra</th>
                                    <th width="150px">Giảm giá (%)</th>
                                    <th width="150px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT * FROM `PromotionEvents`";
                                $result = mysqli_query($conn, $sql);
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $id = $row['id'];
                                    $name = $row['name'];
                                    $start = $row['start_date'];
                                    $end = $row['end_date'];
                                    $desc = $row['description'];
                                    $discount = $row['discount_percentage'];


                                    echo '<tr>
                                                <td class="text-center">' . $id . '</td>
                                                <td>
                                                    <p><b>' . $name . '</b></p>
                                                </td>
                                                <td>
                                                    <p>Ngày bắt đầu : <b>' . $start . '</b></p>
                                                    
                                                    <p>Ngày kết thúc : <b>' . $end . '</b></p>
                                                </td>
                                                <td>
                                                    <p>' . $discount  . '</p>
                                                </td>

                                                <td class="text-center">
                                                        <div class="row mx-auto" style="width:112px">
                                                        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#editUser' . $id . '" type="button">Sửa</button>
                                                            <form action="partials/_evenManage.php" method="POST">
                                                                <button name="removeItem" class="btn btn-sm btn-danger" style="margin-left:9px;">Xóa</button>
                                                                <input type="hidden" name="Id" value="' . $id . '">
                                                            </form>
                                                        </div>
                                                </td>
                                                
                                            </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <!-- Table Panel -->
        </div>
    </div>
</div>



<!-- Create news Modal -->
<div class="modal fade" id="newEvens" tabindex="-1" role="dialog" aria-labelledby="newEvens" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(111 202 203);">
                <h5 class="modal-title" id="newEvens">Tạo sự kiện khuyến mại</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="partials/_evenManage.php" method="post" enctype="multipart/form-data" id="eventForm">
                    <label for="name">Tên sự kiện:</label>
                    <input type="text" name="name" class="form-control" required><br>
                    <div class="row my-3">
                        <div class="col-md-12">
                            <label for="event_type">Dành cho menu:</label>
                            <!-- Thêm dòng input type hidden để lưu giá trị của checkbox -->
                            <input type="hidden" name="applyToMenu" id="applyToMenu" value="0">
                            <input type="checkbox" id="applyToMenuCheckbox" name="applyToMenuCheckbox" onclick="toggleCategorySelectCreate()">
                            <select name="categoryId" id="categoryId" class="custom-select browser-default" select required style="display:none;">

                                <?php
                                $catsql = "SELECT * FROM `categories`";
                                $catresult = mysqli_query($conn, $catsql);
                                while ($row = mysqli_fetch_assoc($catresult)) {
                                    $catId = $row['categorieId'];
                                    $catName = $row['categorieName'];
                                    echo '<option value="' . $catId . '">' . $catName . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <label for="event_type">Loại sự kiện:</label>
                    <select name="event_type" class="form-control" id="event_type" required onchange="toggleDateFieldsCreate()">
                        <option value="loai1">Trong ngày</option>
                        <option value="loai2">Nhiều ngày</option>
                    </select>


                    <div class="row">
                        <div class="col-lg-6">
                            <label for="start_date">Ngày bắt đầu:</label>
                            <input type="datetime-local" name="start_date" class="form-control" id="start_date" required><br>
                        </div>
                        <div class="col-lg-6">
                            <label for="end_date" id="end_date_label" style="display:none;">Ngày kết thúc:</label>
                            <input type="datetime-local" name="end_date" class="form-control" id="end_date" style="display:none;"><br>
                        </div>
                    </div>

                    <label for="description">Mô tả:</label>
                    <textarea name="description" class="form-control"></textarea><br>

                    <label for="discount_percentage">Giảm giá (%):</label>
                    <input type="number" name="discount_percentage" step="0.01" class="form-control" required><br>

                    <button type="submit" name="createEven" class="btn btn-success">Tạo</button>
                </form>

                <script>
                    function toggleDateFieldsCreate() {
                        var eventType = document.getElementById("event_type").value;
                        var startDate = document.getElementById("start_date");
                        var endDateLabel = document.getElementById("end_date_label");
                        var endDate = document.getElementById("end_date");

                        if (eventType === "loai1") {
                            startDate.style.display = "block";
                            endDateLabel.style.display = "none";
                            endDate.style.display = "none";
                        } else if (eventType === "loai2") {
                            startDate.style.display = "block";
                            endDateLabel.style.display = "block";
                            endDate.style.display = "block";
                        }
                    }

                    function toggleCategorySelectCreate() {
                        var applyToMenuCheckbox = document.getElementById("applyToMenuCheckbox");
                        var categoryIdSelect = document.getElementById("categoryId");
                        var applyToMenuInput = document.getElementById("applyToMenu");

                        // Nếu checkbox được chọn, hiển thị ô chọn categoryId; ngược lại, ẩn nó
                        categoryIdSelect.style.display = applyToMenuCheckbox.checked ? "block" : "none";
                        // Gán giá trị của checkbox vào input hidden
                        applyToMenuInput.value = applyToMenuCheckbox.checked ? "1" : "0";
                    }
                </script>
            </div>
        </div>
    </div>
</div>


<?php
$usersql = "SELECT * FROM `promotionevents`";
$userResult = mysqli_query($conn, $usersql);
while ($userRow = mysqli_fetch_assoc($userResult)) {
    $Id = $userRow['id'];
    $name = $userRow['name'];
    $start = $userRow['start_date'];
    $end = $userRow['end_date'];
    $desc = $userRow['description'];
    $discount = $userRow['discount_percentage'];
?>
    <!-- update Modal -->
    <div class="modal fade" id="editUser<?php echo $Id; ?>" tabindex="-1" role="dialog" aria-labelledby="editUser<?php echo $Id; ?>" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: rgb(111 202 203);">
                    <h5 class="modal-title" id="editUser<?php echo $Id; ?>">Id: <b><?php echo $Id; ?></b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="partials/_evenManage.php" method="post" enctype="multipart/form-data" id="eventForm" class="mx-3 mb-3">
                    <label for="nameUpdate">Tên sự kiện:</label>
                    <input type="hidden" name="id" class="form-control" value="<?php echo $Id ?>"><br>
                    <input type="hidden" name="typeSelect" class="form-control" value="<?php echo $desc ?>"><br>
                    <input type="text" name="nameUpdate" class="form-control" value="<?php echo $name ?>"><br>

                    <div class="row">
                        <div class="col-lg-6">
                            <label for="start_date_update">Ngày bắt đầu:</label>
                            <input type="datetime-local" name="start_date_update" class="form-control" id="start_date_update" value="<?php echo $start ?>"><br>
                        </div>
                        <?php
                        if ($end) {
                            echo '<div class="col-lg-6">
                                <label for="end_date_update" id="end_date_update_label">Ngày kết
                                    thúc:</label>
                                <input type="datetime-local" name="end_date_update" class="form-control" id="end_date_update" value="' . $end . '"><br>
                </div>';
                        }
                        ?>
                    </div>

                    <label for=" discount_percentage_update">Giảm giá (%):</label>
                    <input type="number" name="discount_percentage_update" step="0.01" class="form-control" value="<?php echo $discount ?>"><br>

                    <button type="submit" name="updateEven" class="btn btn-success">Tạo</button>
                </form>

                <script>

                </script>
            </div>
        </div>
    </div>
    </div>

<?php
}
?>